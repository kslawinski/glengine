#include "World.h"
#include "Camera.h"
#include "GameObject.h"
#include "Floor.h"
#include "UIObject.h"

World::World()
{
	mainCamera = new Camera();
	//	uiObject = new UIObject();

	// initialize Bullet world
	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);

	solver = new btSequentialImpulseConstraintSolver;
	broadphase = new btDbvtBroadphase();
	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);

	dynamicsWorld->setGravity(btVector3(0, -10, 0));
}

World::~World()
{
	std::cout << "World destroyed!" << std::endl;

	for (int i = 0; i < worldObjects.size(); i++)
	{
		delete worldObjects[i]; // so the object constructor is called and memory cleared
	}
	worldObjects.clear();

	delete solver;
	delete collisionConfiguration;
	delete dispatcher;
	delete broadphase;
	//if(dynamicsWorld)
	//delete dynamicsWorld; // TODO MAKE SURE Dyamics world is deleted now it crashes when this line is executed
}

bool World::CheckCollision(GameObject * objectA, GameObject * objectB)
{
	if (objectA->CheckCollisionWith(*objectB) >= 1)
	{
		return true;
	}
	else
		return false;
}

GameObject * World::CheckCollision(GameObject * objectA)
{
	GameObject* collidingObject = nullptr;

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (objectA->CheckCollisionWith(*worldObjects[i]) >= 1)
		{
			if (worldObjects[i] == objectA) continue;
			collidingObject = worldObjects[i];
		}
	}

	return collidingObject;
}

bool World::DeleteObject(GameObject * gameObject)
{
	bool deleteSuccessed = false;

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (worldObjects[i] == gameObject)
		{
			worldObjects.erase(worldObjects.begin() + i);
			deleteSuccessed = true;
			break;
		}
	}
	//delete gameObject; // so the object constructor is called
	return deleteSuccessed;
}

void World::Update(float deltaTime)
{
	dynamicsWorld->stepSimulation(1 / 60.f, 10); // simulate BULLET PHYSICS

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (worldObjects[i] == nullptr) { continue; }
		worldObjects[i]->Update(deltaTime);

	}

	//std::string text = "Hello world!";

}

GameObject * World::GetClosestObject(GameObject * objectA, float& outDistance)
{
	GameObject* closestObject = nullptr;

	float closestDistance = 10000000;

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (worldObjects[i] == objectA) continue;
		float distanceToObject = glm::distance(objectA->objTransform->GetPosition(), worldObjects[i]->objTransform->GetPosition());

		if (distanceToObject < closestDistance)
		{
			closestObject = worldObjects[i];
			closestDistance = distanceToObject;
		}
	}

	outDistance = closestDistance;

	return closestObject;
}

void World::ScreenPosToWorldRay(
	int mouseX, int mouseY,             // Mouse position, in pixels, from bottom-left corner of the window
	int screenWidth, int screenHeight,  // Window size, in pixels
										//glm::mat4 ViewMatrix,               // Camera position and orientation
										//glm::mat4 ProjectionMatrix,         // Camera parameters (ratio, field of view, near and far planes)
	glm::vec3& out_origin,              // Ouput : Origin of the ray. /!\ Starts at the near plane, so if you want the ray to start at the camera's position instead, ignore this.
	glm::vec3& out_direction            // Ouput : Direction, in world space, of the ray that goes "through" the mouse.
) {

	glm::vec4 lRayStart_NDC(
		((float)mouseX / (float)screenWidth - 0.5f) * 2.0f, // [0,1024] -> [-1,1]
		((float)mouseY / (float)screenHeight - 0.5f) * 2.0f, // [0, 768] -> [-1,1]
		-1.0, // The near plane maps to Z=-1 in Normalized Device Coordinates
		1.0f
	);
	glm::vec4 lRayEnd_NDC(
		((float)mouseX / (float)screenWidth - 0.5f) * 2.0f,
		((float)mouseY / (float)screenHeight - 0.5f) * 2.0f,
		0.0,
		1.0f
	);


	// The Projection matrix goes from Camera Space to NDC.
	// So inverse(ProjectionMatrix) goes from NDC to Camera Space.
	//glm::mat4 InverseProjectionMatrix = glm::inverse(ProjectionMatrix);
	glm::mat4 InverseProjectionMatrix = glm::inverse(mainCamera->GetProjectionMatrix());

	// The View Matrix goes from World Space to Camera Space.
	// So inverse(ViewMatrix) goes from Camera Space to World Space.
	glm::mat4 InverseViewMatrix = glm::inverse(mainCamera->GetViewMatrix());

	glm::vec4 lRayStart_camera = InverseProjectionMatrix * lRayStart_NDC;    lRayStart_camera /= lRayStart_camera.w;
	glm::vec4 lRayStart_world = InverseViewMatrix * lRayStart_camera; lRayStart_world /= lRayStart_world.w;
	glm::vec4 lRayEnd_camera = InverseProjectionMatrix * lRayEnd_NDC;      lRayEnd_camera /= lRayEnd_camera.w;
	glm::vec4 lRayEnd_world = InverseViewMatrix * lRayEnd_camera;   lRayEnd_world /= lRayEnd_world.w;


	// Faster way (just one inverse)
	//glm::mat4 M = glm::inverse(ProjectionMatrix * ViewMatrix);
	//glm::vec4 lRayStart_world = M * lRayStart_NDC; lRayStart_world/=lRayStart_world.w;
	//glm::vec4 lRayEnd_world   = M * lRayEnd_NDC  ; lRayEnd_world  /=lRayEnd_world.w;


	glm::vec3 lRayDir_world(lRayEnd_world - lRayStart_world);
	lRayDir_world = glm::normalize(lRayDir_world);


	out_origin = glm::vec3(lRayStart_world);
	out_direction = glm::normalize(lRayDir_world);
}


void World::WorldToScreenPos(
	int mouseX, int mouseY,             // Mouse position, in pixels, from bottom-left corner of the window
	int screenWidth, int screenHeight,  // Window size, in pixels
										//glm::mat4 ViewMatrix,               // Camera position and orientation
										//glm::mat4 ProjectionMatrix,         // Camera parameters (ratio, field of view, near and far planes)
										//glm::vec3& out_origin,              // Ouput : Origin of the ray. /!\ Starts at the near plane, so if you want the ray to start at the camera's position instead, ignore this.
	glm::vec2& out_ScreenPos,            // Ouput : Direction, in world space, of the ray that goes "through" the mouse.
	glm::vec3 in_Point3D
) {


	// The Projection matrix goes from Camera Space to NDC.
	// So inverse(ProjectionMatrix) goes from NDC to Camera Space.
	//glm::mat4 InverseProjectionMatrix = glm::inverse(ProjectionMatrix);
	glm::mat4 ViewProjectionMatrix = (mainCamera->GetProjectionMatrix() * mainCamera->GetViewMatrix());

	// The ray Start and End positions, in Normalized Device Coordinates (Have you read Tutorial 4 ?)
	glm::vec4 lRayStart_NDC(
		((float)in_Point3D.x),
		((float)in_Point3D.y), // [0, 768] -> [-1,1]
		((float)in_Point3D.z), // The near plane maps to Z=-1 in Normalized Device Coordinates
		1.0f
	);

	glm::vec4 lRayStart_world = ViewProjectionMatrix * lRayStart_NDC;// lRayStart_world/=lRayStart_world.w;
																	 //glm::vec4 lRayEnd_world   = M * lRayEnd_NDC  ; lRayEnd_world  /=lRayEnd_world.w;


																	 //glm::vec3 lRayDir_world(lRayEnd_world - lRayStart_world);
																	 //lRayDir_world = glm::normalize(lRayDir_world);

	int winX = (int)glm::round(((lRayStart_world.x + 1) / 2.0) *
		screenWidth);

	int winY = (int)glm::round(((lRayStart_world.y + 1) / 2.0) *
		screenHeight);

	//out_origin = glm::vec3(lRayStart_world);
	//out_ScreenPos = glm::normalize(lRayDir_world);

	out_ScreenPos = glm::vec2(winX, 0);
}
