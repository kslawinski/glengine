#pragma once
#include "GameObject.h"

#include <BULLET/btBulletDynamicsCommon.h> // BULLET PHISICS

class Floor : public GameObject
{
private:
	btCollisionShape * btObjectCollisionShape;
	btRigidBody* btObjectRB;

public:
	Floor();
	Floor(World* world);
	~Floor();
};