#include "BoxCollider.h"
#include "Vertex.h"


BoxCollider::BoxCollider()
{
}

BoxCollider::BoxCollider(glm::vec3 size, glm::vec3 offset)
{

	//objShader = new Shader();

	//objShader->Bind(); // use shader

	/*

	glm::vec3 ColliderShapeData[36] = { // Create Vertex data objects storing data about vertex positions and colours to be used to create cube object
	glm::vec3(-1.0f,-1.0f,-1.0f),
	glm::vec3(-1.0f,-1.0f, 1.0f),
	glm::vec3(-1.0f, 1.0f, 1.0f),
	glm::vec3(1.0f, 1.0f,-1.0f),
	glm::vec3(-1.0f,-1.0f,-1.0f),
	glm::vec3(-1.0f, 1.0f,-1.0f),

	glm::vec3(1.0f,-1.0f, 1.0f),
	glm::vec3(-1.0f,-1.0f,-1.0f),
	glm::vec3(1.0f,-1.0f,-1.0f),
	glm::vec3(1.0f, 1.0f,-1.0f),
	glm::vec3(1.0f,-1.0f,-1.0f),
	glm::vec3(-1.0f,-1.0f,-1.0f),

	glm::vec3(-1.0f,-1.0f,-1.0f),
	glm::vec3(-1.0f, 1.0f, 1.0f),
	glm::vec3(-1.0f, 1.0f,-1.0f),
	glm::vec3(1.0f,-1.0f, 1.0f),
	glm::vec3(-1.0f,-1.0f, 1.0f),
	glm::vec3(-1.0f,-1.0f,-1.0f),

	glm::vec3(-1.0f, 1.0f, 1.0f),
	glm::vec3(-1.0f,-1.0f, 1.0f),
	glm::vec3(1.0f,-1.0f, 1.0f),
	glm::vec3(1.0f, 1.0f, 1.0f),
	glm::vec3(1.0f,-1.0f,-1.0f),
	glm::vec3(1.0f, 1.0f,-1.0f),

	glm::vec3(1.0f,-1.0f,-1.0f),
	glm::vec3(1.0f, 1.0f, 1.0f),
	glm::vec3(1.0f,-1.0f, 1.0f),
	glm::vec3(1.0f, 1.0f, 1.0f),
	glm::vec3(1.0f, 1.0f,-1.0f),
	glm::vec3(-1.0f, 1.0f,-1.0f),

	glm::vec3(1.0f, 1.0f, 1.0f),
	glm::vec3(-1.0f, 1.0f,-1.0f),
	glm::vec3(-1.0f, 1.0f, 1.0f),
	glm::vec3(1.0f, 1.0f, 1.0f),
	glm::vec3(-1.0f, 1.0f, 1.0f),
	glm::vec3(1.0f,-1.0f, 1.0f)

	};
	*/
	Vertex CubeData[36] = { // Create Vertex data objects storing data about vertex positions and colours to be used to create cube object
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000103f, 1.0f - 0.336048f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(1.000023f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.999958f, 1.0f - 0.336064f)),

		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667969f, 1.0f - 0.671889f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(1.000023f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.668104f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),

		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.336098f, 1.0f - 0.000071f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),

		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(1.000004f, 1.0f - 0.671847f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.999958f, 1.0f - 0.336064f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.668104f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),

		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.668104f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.336098f, 1.0f - 0.000071f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000103f, 1.0f - 0.336048f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000004f, 1.0f - 0.671870f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),

		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000103f, 1.0f - 0.336048f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667969f, 1.0f - 0.671889f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(1.000004f, 1.0f - 0.671847f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),

	};

	verticesCount = 36;

	for (unsigned int i = 0; i < 36; i++)
	{
		//verticesColour.push_back(glm::vec3(0, 1, 0));
		CubeData[i].colour = glm::vec3(0.0f, 1.0f, 0.0f); // set all vertex color to green
		CubeData[i].position *= size;
		CubeData[i].position += offset;
	}

	objMesh = new Mesh(CubeData, 36);
	objMesh->isWireFrame = true;
	/*

	glGenBuffers(1, &vertices);
	//make vertices the ‘current’ array buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertices);
	//send data to buffer on GPU
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*verticesCount, &ColliderShapeData[0], GL_STATIC_DRAW);

	glGenBuffers(1, &colours);
	//GLfloat colourData[] = {1,0,0,0,1,0, 0,0,1};
	glBindBuffer(GL_ARRAY_BUFFER, colours);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*verticesCount, &verticesColour[0], GL_STATIC_DRAW);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(colourData), colourData, GL_STATIC_DRAW);

	glGenVertexArrays(1, &vaoHandle);   	//generate VAO
	glBindVertexArray(vaoHandle);       	//only one target for VAOs - no need to specify

	glBindBuffer(GL_ARRAY_BUFFER, vertices);
	glEnableVertexAttribArray(0);       		//Allows shaders to read attribute 0
	glEnableVertexAttribArray(1);
	glBindVertexBuffer(0, vertices, 0, sizeof(GLfloat) * 3);   //vertex data is now attribute 0
	glBindVertexBuffer(1, colours, 0, sizeof(GLfloat) * 3);
	glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(1, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribBinding(0, 0);
	glVertexAttribBinding(1, 1);
	*/
}


BoxCollider::~BoxCollider()
{
	/*
	glDeleteBuffers(1, &vertices);
	glDeleteVertexArrays(1, &vertices);
	glDeleteBuffers(1, &colours);
	glDeleteVertexArrays(1, &colours);
	*/
}

void BoxCollider::Update(glm::mat4 modelMatrix)
{

	objMesh->Draw();

}
