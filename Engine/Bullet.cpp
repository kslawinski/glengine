#include "Bullet.h"
#include "World.h"
#include "Material.h"

Bullet::Bullet()
{
}

Bullet::Bullet(World* world)
{
	Init(*world->mainCamera);

	Vertex CubeData[36] = { // Create Vertex data objects storing data about vertex positions and colours to be used to create cube object
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.2f, 1.0f)),
		Vertex(glm::vec3(-0.4f,-0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.2f, 0.2f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.2f, 0.3f)),
		Vertex(glm::vec3(0.4f, 0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.1f, 1.0f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.2f, 1.0f)),
		Vertex(glm::vec3(-0.4f, 0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.3f, 1.0f)),
		Vertex(glm::vec3(0.4f, 0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.3f, 1.0f)),
		Vertex(glm::vec3(0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.2f, 1.0f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.2f, 1.0f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f)),
		Vertex(glm::vec3(-0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f)),

		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f)),
		Vertex(glm::vec3(-0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.2f, 1.0f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f)),
		Vertex(glm::vec3(-0.4f,-0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f)),
		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.4f),glm::vec2(0.3f,  0.4f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.3f,  0.4f)),
		Vertex(glm::vec3(0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.9f,  0.4f)),
		Vertex(glm::vec3(0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.2f, 0.4f)),
		Vertex(glm::vec3(0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.2f,  0.000004f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.59f,  0.4f)),
		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.59f,  0.4f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.9f,  0.5f)),
		Vertex(glm::vec3(0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 0.4f)),
		Vertex(glm::vec3(-0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.2f,0.5f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f)),
		Vertex(glm::vec3(-0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.29f, 1.0f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),

	};

	objMesh = new Mesh(CubeData, 36); // create Cube using data stored in Vertex objects
	objTransform->SetScale(glm::vec3(0.1f, 0.1f, 0.1f));
	objTransform->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));

	//SetMaterial(new Material());

	objectTag = "Bullet";
}

void Bullet::Update(float deltaTime)
{
	GameObject::Update(deltaTime);

	glm::vec3 newLocation = objTransform->GetPosition();

	bulletVelocity += bulletDirection * (deltaTime * bulletSpeed);

	//std::cout << velocity.y << std::endl;

	if (bulletVelocity != glm::vec3(0.0f, 0.0f, 0.0f))
	{
		newLocation = objTransform->GetPosition() + (bulletVelocity * deltaTime);
		objTransform->SetPosition(glm::vec3(newLocation));
	}
}

void Bullet::Setup(glm::vec3 direction, float speed)
{
	bulletDirection = direction;
	bulletSpeed = speed;
}


Bullet::~Bullet()
{
}