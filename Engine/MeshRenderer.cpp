#include "MeshRenderer.h"
#include "Material.h"
#include "Mesh.h"

MeshRenderer::MeshRenderer()
{
}

MeshRenderer::MeshRenderer(Camera * camera)
{
	this->camera = camera;

	glGenBuffers(1, &vertices);
	glGenBuffers(1, &colours);
	glGenBuffers(1, &normals);
	glGenBuffers(1, &uvbuffer);

	glGenVertexArrays(1, &vaoHandle);   	//generate VAO

	objMaterial = new Material();
	objTransform = new Transform();
	//printf("Camera INIT!!");
	objMaterial->materialShader->Bind();
}


MeshRenderer::~MeshRenderer()
{

}

void MeshRenderer::Render(StaticMesh& meshToRender)
{
	int vertCount = meshToRender.verticesPosition.size();
	//meshToRender.objMaterial->materialShader->Bind();
	objMaterial->materialShader->Bind();

	view = camera->GetViewMatrix();
	model = objTransform->GetModel();

	//if (meshToRender.objMaterial == nullptr) { return; }

	GLuint MVid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "ModelView");		//get handle
	glUniformMatrix4fv(MVid, 1, GL_FALSE, &modelView[0][0]);			//link it to mvp matrix

	GLuint Normid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Norm");		//get handl
	glUniformMatrix4fv(Normid, 1, GL_FALSE, &normMatrix[0][0]);			//link it to mvp matrix

	GLuint MVPid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "MVP");		//get handle
	glUniformMatrix4fv(MVPid, 1, GL_FALSE, &MVP[0][0]);			//link it to mvp matrix

																// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, objMaterial->materialTexture);
	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "myTextureSampler");
	// Set our "myTextureSampler" sampler to use Texture Unit 0
	glUniform1i(TextureID, 0);

	MVP = proj * view*model; // refresh the transformations
							 //meshToRender.objMaterial->materialShader->Bind();

	int verticesCount = meshToRender.verticesCount;

	//make vertices the ‘current’ array buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertices);
	//send data to buffer on GPU
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*vertCount, &meshToRender.verticesPosition[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, colours);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*vertCount, &meshToRender.verticesColour[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*vertCount, &meshToRender.verticesPosition[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)*vertCount, &meshToRender.verticesUV[0], GL_STATIC_DRAW);

	glBindVertexArray(vaoHandle);       	//only one target for VAOs - no need to specify

	glEnableVertexAttribArray(0);       		//Allows shaders to read attribute 0
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glBindVertexBuffer(0, vertices, 0, sizeof(GLfloat) * 3);   //vertex data is now attribute 0
	glBindVertexBuffer(1, colours, 0, sizeof(GLfloat) * 3);
	glBindVertexBuffer(2, normals, 0, sizeof(GLfloat) * 3);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		3,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(1, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(2, 3, GL_FLOAT, GL_FALSE, 0); // not normalised
	glVertexAttribFormat(3, 3, GL_FLOAT, GL_FALSE, 0); // not normalised
	glVertexAttribBinding(0, 0);
	glVertexAttribBinding(1, 1);
	glVertexAttribBinding(2, 2);
	glVertexAttribBinding(3, 3);

	////////////////////////////////////////////////////////////////////////////////

	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(0);

	glBindVertexArray(vaoHandle);		//Use VAO

	glDrawArrays(GL_TRIANGLES, 0, vertCount);	//render the vertices

	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
}
