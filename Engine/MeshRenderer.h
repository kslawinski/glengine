#pragma once
#include "StaticMesh.h"
#include "Camera.h"

class MeshRenderer
{
public:
	GLuint vertices; // Declare Special variables to be used to send data to GPU
	GLuint colours;
	GLuint normals;
	GLuint uvbuffer;
	GLuint vaoHandle;

	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 proj;

	glm::mat4 MVP;
	glm::mat4 modelView;
	glm::mat3 normMatrix;


	Material* objMaterial;
	Transform* objTransform;

	Camera* camera;

	MeshRenderer();
	MeshRenderer(Camera* camera);
	~MeshRenderer();

	void Render(StaticMesh& meshToRender);
};