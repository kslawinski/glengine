#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <iostream>
#include "GameObject.h"
#include <BULLET/btBulletDynamicsCommon.h>

class GameObject;
class Camera;
class Floor;
class UIObject;

class World
{
public:

	World();
	~World();

	// BULLET PHYSICS
	btDiscreteDynamicsWorld* dynamicsWorld; // pointer for bullet dynamic world
	btDefaultCollisionConfiguration* collisionConfiguration; // pointer for world collision configuration
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;
	btBroadphaseInterface* broadphase;

	template <class T> T* Instantiate();

	template <class T> T* Instantiate(glm::vec3 position);

	std::vector<GameObject*> worldObjects;

	bool CheckCollision(GameObject* objectA, GameObject* objectB);
	GameObject* CheckCollision(GameObject* objectA);

	bool DeleteObject(GameObject* gameObject);

	void Update(float deltaTime);

	Camera* mainCamera;

	int playerPoints = 0;



	GameObject* GetClosestObject(GameObject* objectA, float& outDistance);
	void ScreenPosToWorldRay(int mouseX, int mouseY, int screenWidth, int screenHeight, glm::vec3 & out_origin, glm::vec3 & out_direction);
	void WorldToScreenPos(int mouseX, int mouseY, int screenWidth, int screenHeight, glm::vec2 & out_ScreenPos, glm::vec3 in_Point3D);
	//void WorldToScreenPos(int mouseX, int mouseY, int screenWidth, int screenHeight, glm::vec2 & out_ScreenPos, glm::vec3 & in_Point3D);

};

template<class T>
inline T* World::Instantiate()
{
	T* t = new T(this);
	worldObjects.push_back(t);
	worldObjects.back()->Draw();// redraw the object
	std::cout << t->objectTag << std::endl;
	return t;
}

template<class T>
inline T * World::Instantiate(glm::vec3 position)
{
	T* t = new T(this);
	t->objTransform->SetPosition(position);
	worldObjects.push_back(t);
	worldObjects.back()->Draw();
	std::cout << t->objectTag << std::endl;
	return t;
}
