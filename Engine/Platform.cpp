#include "Platform.h"
#include "Material.h"


Platform::Platform()
{
}

Platform::Platform(World * world)
{
	Init(*world->mainCamera);
	//worldInstance = world;

	//	myTexture = loadBMP_custom("uvtemplatemy.bmp");

	Vertex CubeData[36] = { // Create Vertex data objects storing data about vertex positions and colours to be used to create cube object
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.1f,  0.5f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f, 0.5f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f, 0.5f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f, 0.5f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f, 0.5f)),

		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f,0.5f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f, 0.5f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f,  0.5)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,1.0f),glm::vec2(0.5f, 0.5)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f, 0.5)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,1.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f, 0.5f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5,0.5)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f, 0.5f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5, 0.5)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.5f,  0.5f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.5f,  0.5f)),

	};

	objMesh = new Mesh(CubeData, 36); // create Cube using data stored in Vertex objects
	objTransform->SetScale(glm::vec3(1.0f, 0.3f, 1.0f));
	//objTransform->SetPosition(glm::vec3(0.0f, -0.5, 0.0f));
	//SetMaterial(new Material("uvtemplatemy.bmp"));

	objectTag = "Platform";
}


Platform::~Platform()
{
}
