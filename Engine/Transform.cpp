#include "Transform.h"



Transform::Transform()
{
	scale = glm::vec3(1, 1, 1);
}


Transform::~Transform()
{
}

glm::vec3 Transform::GetPosition()
{
	return position;
}

glm::vec3 Transform::GetEulerAngleRotation()
{
	return eulerAngles;
}

glm::quat Transform::GetRotationQuat()
{
	glm::quat rotationQuat = glm::quat(eulerAngles);

	//rotation = glm::angleAxis(360,)

	return rotationQuat;
}

glm::vec3 Transform::GetScale()
{
	return scale;
}

void Transform::SetEulerAnglesRotation(glm::vec3 rotation)
{
	this->eulerAngles = rotation;
	this->rotation = glm::quat(eulerAngles);
}

void Transform::RotateYaw(float rotation)
{
	yawAngle += rotation;
	this->eulerAngles.y += rotation;
	this->rotation = glm::quat(eulerAngles);
}

void Transform::RotatePitch(float rotation)
{
	pitchAngle += rotation;
	this->eulerAngles.x += rotation;
	this->rotation = glm::quat(eulerAngles);
}

void Transform::SetPosition(glm::vec3 position)
{
	this->position = position;
}

void Transform::SetScale(glm::vec3 scale)
{
	this->scale = scale;
}

glm::mat4 Transform::GetModel()
{
	glm::mat4 positionMatrix = glm::translate(glm::mat4(1.0), position);
	//glm::mat4 rotationXMatrix = glm::rotate(glm::mat4(1.0), eulerAngles.x, glm::vec3(1, 0, 0));
	//glm::mat4 rotationYMatrix = glm::rotate(glm::mat4(1.0), eulerAngles.y, glm::vec3(0, 1, 0));
	//glm::mat4 rotationZMatrix = glm::rotate(glm::mat4(1.0), eulerAngles.z, glm::vec3(0, 0, 1));
	glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0), scale);

	glm::mat4 rotationMatrix = glm::mat4_cast(rotation);//glm::eulerAngleYXZ(eulerAngles.y, eulerAngles.x, eulerAngles.z);//rotationZMatrix * rotationYMatrix * rotationXMatrix;

	glm::mat4 modelMatrix = positionMatrix * rotationMatrix * scaleMatrix;

	return modelMatrix;
}

glm::vec3 Transform::GetRightVector()
{
	glm::vec3 rightVector = glm::vec3(
		sin(yawAngle - 3.14f / 2.0f),
		0,
		cos(pitchAngle - 3.14f / 2.0f)
	);

	return rightVector;
}

glm::vec3 Transform::GetDirection()
{
	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
		cos(pitchAngle) * sin(yawAngle),
		sin(pitchAngle),
		cos(pitchAngle) * cos(yawAngle)
	);

	return direction;
}

//TODO: IMPLEMENT DIRECTION !!!

// Returns a quaternion such that q*start = dest
glm::quat Transform::RotationBetweenVectors(glm::vec3 start, glm::vec3 dest) {
	start = glm::normalize(start);
	dest = glm::normalize(dest);

	float cosTheta = glm::dot(start, dest);
	glm::vec3 rotationAxis;

	if (cosTheta < -1 + 0.001f) {
		// special case when vectors in opposite directions :
		// there is no "ideal" rotation axis
		// So guess one; any will do as long as it's perpendicular to start
		// This implementation favors a rotation around the Up axis,
		// since it's often what you want to do.
		rotationAxis = glm::cross(glm::vec3(0.0f, 0.0f, 1.0f), start);
		if (length2(rotationAxis) < 0.01) // bad luck, they were parallel, try again!
			rotationAxis = glm::cross(glm::vec3(1.0f, 0.0f, 0.0f), start);

		rotationAxis = glm::normalize(rotationAxis);
		return angleAxis(glm::radians(180.0f), rotationAxis);
	}

	// Implementation from Stan Melax's Game Programming Gems 1 article
	rotationAxis = glm::cross(start, dest);

	float s = sqrt((1 + cosTheta) * 2);
	float invs = 1 / s;

	return glm::quat(
		s * 0.5f,
		rotationAxis.x * invs,
		rotationAxis.y * invs,
		rotationAxis.z * invs
	);


}



// Returns a quaternion that will make your object looking towards 'direction'.
// Similar to RotationBetweenVectors, but also controls the vertical orientation.
// This assumes that at rest, the object faces +Z.
// Beware, the first parameter is a direction, not the target point !
glm::quat Transform::LookAt(glm::vec3 direction, glm::vec3 desiredUp) {

	if (length2(direction) < 0.0001f)
		return glm::quat();

	// Recompute desiredUp so that it's perpendicular to the direction
	// You can skip that part if you really want to force desiredUp
	glm::vec3 right = glm::cross(direction, desiredUp);
	desiredUp = cross(right, direction);

	// Find the rotation between the front of the object (that we assume towards +Z,
	// but this depends on your model) and the desired direction
	glm::quat rot1 = RotationBetweenVectors(glm::vec3(0.0f, 0.0f, 1.0f), direction);
	// Because of the 1rst rotation, the up is probably completely screwed up. 
	// Find the rotation between the "up" of the rotated object, and the desired up
	glm::vec3 newUp = rot1 * glm::vec3(0.0f, 1.0f, 0.0f);
	glm::quat rot2 = RotationBetweenVectors(newUp, desiredUp);

	// Apply them
	return rot2 * rot1; // remember, in reverse order.
}



// Like SLERP, but forbids rotation greater than maxAngle (in radians)
// In conjunction to LookAt, can make your characters 
glm::quat Transform::RotateTowards(glm::quat q1, glm::quat q2, float maxAngle) {

	if (maxAngle < 0.001f) {
		// No rotation allowed. Prevent dividing by 0 later.
		return q1;
	}

	float cosTheta = glm::dot(q1, q2);

	// q1 and q2 are already equal.
	// Force q2 just to be sure
	if (cosTheta > 0.9999f) {
		return q2;
	}

	// Avoid taking the long path around the sphere
	if (cosTheta < 0) {
		q1 = q1 * -1.0f;
		cosTheta *= -1.0f;
	}

	float angle = acos(cosTheta);

	// If there is only a 2� difference, and we are allowed 5�,
	// then we arrived.
	if (angle < maxAngle) {
		return q2;
	}

	// This is just like slerp(), but with a custom t
	float t = maxAngle / angle;
	angle = maxAngle;

	glm::quat res = (sin((1.0f - t) * angle) * q1 + sin(t * angle) * q2) / sin(angle);
	res = glm::normalize(res);
	return res;

}