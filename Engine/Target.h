#pragma once
#include "GameObject.h"
class Target : public GameObject
{
private:
	float hp;
public:
	Target();
	Target(World* world);
	void Update(float deltaTime) override;

	void GiveDamage(float damage);

	float damgeCounter;
	float damageCounterSave;
	~Target();
};