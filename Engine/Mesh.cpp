#include "Mesh.h"
#include "Vertex.h"


Mesh::Mesh(Vertex * verticesObj, unsigned int verticesCount)
{
	//std::vector<glm::vec3> verticesPosition;

	//std::vector<glm::vec3> verticesColour;
	//std::vector<glm::vec2> verticesUV;

	this->verticesCount = verticesCount;

	for (unsigned int i = 0; i< verticesCount; i++) // assign Vertex information to vertexPosition and colour Vector arrays
	{
		verticesPosition.push_back(verticesObj[i].position);
		verticesColour.push_back(verticesObj[i].colour);
		verticesUV.push_back(verticesObj[i].uvCord);
	}

	glGenBuffers(1, &vertices);
	glGenBuffers(1, &colours);
	glGenBuffers(1, &normals);
	glGenBuffers(1, &uvbuffer);

	glGenVertexArrays(1, &vaoHandle);   	//generate VAO

	meshBounds = new ObjectBounds(verticesPosition);

	isWireFrame = false;
}

void Mesh::Setup()
{
}

void Mesh::Draw()
{
	//make vertices the ‘current’ array buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertices);
	//send data to buffer on GPU
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*verticesCount, &verticesPosition[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, colours);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*verticesCount, &verticesColour[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*verticesCount, &verticesPosition[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)*verticesCount, &verticesUV[0], GL_STATIC_DRAW);

	glBindVertexArray(vaoHandle);       	//only one target for VAOs - no need to specify

	glEnableVertexAttribArray(0);       		//Allows shaders to read attribute 0
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glBindVertexBuffer(0, vertices, 0, sizeof(GLfloat) * 3);   //vertex data is now attribute 0
	glBindVertexBuffer(1, colours, 0, sizeof(GLfloat) * 3);
	glBindVertexBuffer(2, normals, 0, sizeof(GLfloat) * 3);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		3,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(1, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(2, 3, GL_FLOAT, GL_FALSE, 0); // not normalised
	glVertexAttribBinding(0, 0);
	glVertexAttribBinding(1, 1);
	glVertexAttribBinding(2, 2);

	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(0);

	glBindVertexArray(vaoHandle);		//Use VAO

	if (isWireFrame == false)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	glDrawArrays(GL_TRIANGLES, 0, verticesCount);	//render the vertices

	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);

}

void Mesh::SetMeshColour(glm::vec3 colour)
{
	for (unsigned int i = 0; i < verticesColour.size(); i++)
	{
		verticesColour[i] = colour;
	}
}

void Mesh::Test()
{
}

Mesh::Mesh()
{
}


Mesh::~Mesh()
{
	glDeleteBuffers(1, &vertices);
	glDeleteVertexArrays(1, &vertices);
	glDeleteBuffers(1, &colours);
	glDeleteVertexArrays(1, &colours);
	glDeleteBuffers(1, &normals);
	glDeleteVertexArrays(1, &normals);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteVertexArrays(1, &uvbuffer);
}