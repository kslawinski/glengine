#include "Camera.h"
#include <iostream>
#include <cstdio>


Camera::Camera()
{
	camTransform.SetPosition(glm::vec3(0.0f, 0.0f, 7.0f)); // Variable to store the Camera position
	camTarget = glm::vec3(0.0f, 0.0f, 0.0f); // variable storing the camera looking at target position
	camUp = glm::vec3(0.0f, 1.0f, 0.0f); // Tell the camera which way is up in this case Y is up

										 //std::cout << "RightVector camera: " << camTransform.GetDirection().z << std::endl;
}


Camera::~Camera()
{
}

glm::mat4 Camera::GetViewMatrix()
{
	glm::mat4 view = glm::lookAt(camTransform.GetPosition(), // Eye position (Camera Position in the world)
		glm::vec3(camTransform.GetPosition() + camTransform.GetDirection()),//camTarget, //      Where the camera is looking at POINTTARGET position + direction
		camUp); //     Which way is up
	return view;
}

glm::mat4 Camera::GetProjectionMatrix()
{
	glm::mat4 projectionMatrix = glm::perspective(45.f, 4.f / 3.f, 0.2f, 50.0f);

	return projectionMatrix;
}

void Camera::RotateCameraY(float value)
{


	//camTransform.SetRotation(camTransform.GetRotation() + glm::vec3(0.0f, 0.1f, 0.0f));

	//float camX = sin(value) * 40.0f;
	//float camZ = cos(value) * 40.0f;

	//camTransform.SetRotation(glm::vec3(sin(value) * 40.0f,0.0f, cos(value) * 40.0f));

	camTransform.RotateYaw(-value);
	//float camZ = cos(value) * 40.0f;

	//camTarget = glm::vec3(camX, 0, camZ);
	//camTarget = camTransform.GetRotation();

	//view = glm::lookAt(CamPosition, CamTarget, CamUp);

	//std::cout << "camera direction x: " << camTransform.GetDirection().x << " camera direction: y " << camTransform.GetDirection().y << "camera direction: z " << camTransform.GetDirection().z << std::endl;
}

void Camera::RotateCameraX(float value)
{
	//camTransform.SetRotation(glm::vec3(0.0f,sin(value) * 40.0f, cos(value) * 40.0f));
	//float camZ = cos(value) * 40.0f;

	//camTarget = glm::vec3(camX, 0, camZ);
	//camTarget = camTransform.GetRotation();
	camTransform.RotatePitch(-value);
}

void Camera::MoveForward(float value)
{
	camTransform.SetPosition(camTransform.GetPosition() -= glm::vec3(0, 0, value));
	camTarget -= glm::vec3(0, 0, value);
}

void Camera::MoveRight(float value)
{
	camTransform.SetPosition(camTransform.GetPosition() += glm::vec3(value, 0, 0));
	camTarget += glm::vec3(value, 0, 0);
}

void Camera::MoveUp(float value)
{
	camTransform.SetPosition(camTransform.GetPosition() += glm::vec3(0, value, 0));
	camTarget += glm::vec3(0, value, 0);
}

