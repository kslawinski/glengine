#pragma once
#include <glm/glm.hpp>
// Include GLFW
// Include GLEW
#include <GLEW/glew.h>
#include <GLFW\glfw3.h>
#include "Shader.h"

class Material
{
public:
	GLuint materialTexture;
	glm::vec3 materialColor;
	Shader* materialShader;

	Material();
	Material(std::string filePath);
	~Material();

	void Bind();

	GLuint loadBMP_custom(const char * imagepath);
};

