#pragma once
#include <glm/glm.hpp>
#include "Vertex.h"
#include "Shader.h"
#include "Mesh.h"
//#include "texture.h"
#include "Transform.h"

#include <vector>
#include <cstring>
#include <iostream>
#include "World.h"

class Camera;
class World;
class Material;

class GameObject
{
protected:
	void Init(Camera& camera);

	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 proj;

	glm::mat4 MVP;
	glm::mat4 modelView;
	glm::mat3 normMatrix;

	Camera* camera;
	World* worldInstance;

public:
	GameObject(World* world);
	GameObject();
	~GameObject();

	virtual void Update(float deltaTime);

	virtual void Draw();

	Shader* objShader;

	Material* objMaterial;

	Transform*  objTransform;
	Mesh* objMesh;
	//Texture* objTexture;// (".bricks.jpg");

						//GLuint myTexture;

	std::string objectTag;

	glm::mat4 GetModelMatrix();

	void SetMaterial(Material* mat);

	int CheckCollisionWith(GameObject & other);

	GLuint loadBMP_custom(const char * imagepath);

	bool loadOBJ(const char * path, std::vector<glm::vec3> & out_vertices, std::vector<glm::vec2> & out_uvs, std::vector<glm::vec3> & out_normals
	);
	bool TestRayOBBIntersection(glm::vec3 ray_origin, glm::vec3 ray_direction, glm::vec3 aabb_min, glm::vec3 aabb_max, glm::mat4 ModelMatrix, float & intersection_distance);
};

