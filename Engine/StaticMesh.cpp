#include "StaticMesh.h"
#include "Mesh.h"
#include "Material.h"
#include "Vertex.h"
#include "Transform.h"

StaticMesh::StaticMesh()
{
	Vertex CubeData[36] = { // Create Vertex data objects storing data about vertex positions and colours to be used to create cube object
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000103f, 1.0f - 0.336048f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(1.000023f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.999958f, 1.0f - 0.336064f)),

		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667969f, 1.0f - 0.671889f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(1.000023f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.668104f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),

		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.336098f, 1.0f - 0.000071f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),

		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(1.000004f, 1.0f - 0.671847f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.999958f, 1.0f - 0.336064f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.668104f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),

		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.668104f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.336098f, 1.0f - 0.000071f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000103f, 1.0f - 0.336048f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000004f, 1.0f - 0.671870f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),

		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000103f, 1.0f - 0.336048f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667969f, 1.0f - 0.671889f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(1.000004f, 1.0f - 0.671847f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),

	};
	objMesh = new Mesh(CubeData, 36); // create Cube using data stored in Vertex objects

	objMaterial = new Material();
	objTransform = new Transform();

	Setup();
}

StaticMesh::~StaticMesh()
{
}

void StaticMesh::Setup()
{
	if (objMesh == nullptr) { std::cout << "StaticMesh::Setup NO MESH ASSIGNED!" << std::endl; return; }


	verticesPosition = objMesh->verticesPosition;
	verticesColour = objMesh->verticesColour;
	verticesUV = objMesh->verticesUV;

	verticesCount = objMesh->verticesPosition.size();

	vertices = objMesh->vertices;
	normals = objMesh->normals;
	uvbuffer = objMesh->uvbuffer;
	colours = objMesh->colours;

}

void StaticMesh::SetMaterial(Material & material)
{
	objMaterial = &material;
}

void StaticMesh::SetMesh(Mesh & mesh)
{
	objMesh = &mesh;
	Setup();
}

void StaticMesh::Draw()
{
	//make vertices the ‘current’ array buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertices);
	//send data to buffer on GPU
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*verticesCount, &verticesPosition[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, colours);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*verticesCount, &verticesColour[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*verticesCount, &verticesPosition[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)*verticesCount, &verticesUV[0], GL_STATIC_DRAW);

	glBindVertexArray(vaoHandle);       	//only one target for VAOs - no need to specify

	glEnableVertexAttribArray(0);       		//Allows shaders to read attribute 0
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glBindVertexBuffer(0, vertices, 0, sizeof(GLfloat) * 3);   //vertex data is now attribute 0
	glBindVertexBuffer(1, colours, 0, sizeof(GLfloat) * 3);
	glBindVertexBuffer(2, normals, 0, sizeof(GLfloat) * 3);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		3,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(1, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(2, 3, GL_FLOAT, GL_FALSE, 0); // not normalised
	glVertexAttribBinding(0, 0);
	glVertexAttribBinding(1, 1);
	glVertexAttribBinding(2, 2);




	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(0);

	glBindVertexArray(vaoHandle);		//Use VAO

	glDrawArrays(GL_TRIANGLES, 0, verticesCount);	//render the vertices

	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);

}
