#pragma once
// Include GLEW
#include <GLEW/glew.h>

// Include GLFW
#include <GLFW\glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp> // Maths library

#include <vector>
//#include "Shader.h"
#include "Mesh.h"

class BoxCollider
{
private:
	std::vector<glm::vec3> verticesPosition;
	std::vector<glm::vec3> verticesColour;
	unsigned int verticesCount;

	class Shader* objShader;

	GLuint vertices;
	GLuint colours;
	GLuint vaoHandle;

	Mesh* objMesh;
public:
	BoxCollider();
	BoxCollider(glm::vec3 size, glm::vec3 offset);
	~BoxCollider();

	void Update(glm::mat4 modelMatrix);
};