#include "Target.h"
#include "World.h"
#include "Material.h"
Target::Target()
{
}

Target::Target(World* world)
{
	Init(*world->mainCamera);
	worldInstance = world;

	Vertex CubeData[36] = { // Create Vertex data objects storing data about vertex positions and colours to be used to create cube object
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.2f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.3f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.1f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),

		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,1.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,1.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),

	};

	objMesh = new Mesh(CubeData, 36); // create Cube using data stored in Vertex objects
	objTransform->SetScale(glm::vec3(1.0f, 1.0f, 0.2f));
	//objTransform->SetPosition(glm::vec3(0.0f, 1.0f, -10.0f));

	SetMaterial(new Material("EngineContent/Textures/uvtemplatemy.bmp"));

	objectTag = "Target";

	hp = 100;
	damageCounterSave = 1.2f;
	damgeCounter = damageCounterSave;
}

void Target::Update(float deltaTime)
{
	GameObject::Update(deltaTime);

	/*
	if (GameObject*collidingObject = worldInstance->CheckCollision(this))
	{
	if (collidingObject != nullptr) {
	std::cout << "Colliding with target!!: " << "tag: " << collidingObject->objectTag << std::endl;
	if (collidingObject->objectTag == "Bullet")
	{

	worldInstance->DeleteObject(collidingObject);
	//collidingObject = nullptr;
	//delete collidingObject;
	//break;
	}
	}

	//worldObjects.pop_back();
	}*/
	float distance = 0;
	GameObject* closestObject = worldInstance->GetClosestObject(this, distance);

	if (closestObject != nullptr)
	{
		if (closestObject->objectTag == "Bullet")
		{
			if (distance <= 0.6f) // TODO THE BOUNDS ARE NOT UPDATED AFTER MODEL RESCALED!!
			{
				if (worldInstance->CheckCollision(this))
				{
					std::cout << "Colliding with target!!: " << "tag: " << closestObject->objectTag << "Distance: " << distance << std::endl;
					worldInstance->DeleteObject(closestObject);
					damgeCounter = damageCounterSave;
					GiveDamage(25);
				}
			}
		}
	}
	float colsine = 0;
	if (damgeCounter > 0)
	{
		damgeCounter -= deltaTime;
		//std::cout << "DamageCounter " << damgeCounter << std::endl;
		colsine = glm::sin(damgeCounter * 3);

		//std::cout << "colsine: " << colsine << std::endl;
		objMesh->SetMeshColour(glm::vec3(1, colsine, 1));
	}
	else if (damgeCounter <= 0 && damgeCounter > -6.0f && colsine <= 0.1f)
	{
		objMesh->SetMeshColour(glm::vec3(1, 1, 1));
	}
}

void Target::GiveDamage(float damage)
{
	hp -= damage;
	if (hp <= 0)
	{
		std::cout << "DEAD!" << std::endl;
		worldInstance->DeleteObject(this);
		worldInstance->playerPoints += 10; // inrease player points
	}
	return;
}


Target::~Target()
{
}