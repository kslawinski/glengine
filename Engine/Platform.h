#pragma once
#include "GameObject.h"
class Platform :
	public GameObject
{
public:
	Platform();
	Platform(World* world);
	~Platform();
};