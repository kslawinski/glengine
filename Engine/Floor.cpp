#include "Floor.h"
#include "Camera.h"
#include "World.h"
#include "Material.h"

Floor::Floor()
{
}

Floor::Floor(World* world)
{
	Init(*world->mainCamera);

	worldInstance = world;

	objectTag = "Environment";

	Vertex FloorData[6] = {
		Vertex(glm::vec3(10.0f, -1.0f, 10.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(1.0f,0.0f)),
		Vertex(glm::vec3(10.0f, -1.0f,-10.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(1.0f, 1.0f)),
		Vertex(glm::vec3(-10.0f, -1.0f,-10.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(10.0f, -1.0f, 10.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(-10.0f, -1.0f,-10.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(1.0f,  0.0f)),
		Vertex(glm::vec3(-10.0f, -1.0f, 10.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(1.0f, 1.0f)),

	};

	objMesh = new Mesh(FloorData, 6); // Create Mesh Object and use The vertex and colour data for creation)
	objMesh->SetMeshColour(glm::vec3(1, 1, 1));
	SetMaterial(new Material("EngineContent/Textures/floorUVmap.bmp"));
	//myTexture = loadBMP_custom("floorUVmap.bmp");
	//myTexture = loadBMP_custom("uvtemplate.bmp");

	//BULLET
	btObjectCollisionShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1); // set the collision shape of the floor

	btDefaultMotionState* groundMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -2, 0))); // change -2 for floor hight
	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0, groundMotionState, btObjectCollisionShape, btVector3(0, 0, 0));
	btObjectRB = new btRigidBody(groundRigidBodyCI);
	worldInstance->dynamicsWorld->addRigidBody(btObjectRB);
}


Floor::~Floor()
{

	worldInstance->dynamicsWorld->removeRigidBody(btObjectRB);
	delete btObjectRB->getMotionState();
	delete btObjectRB;

	delete btObjectCollisionShape;
}