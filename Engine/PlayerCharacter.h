#pragma once
#include "GameObject.h"
class PlayerCharacter : public GameObject
{
private:
	glm::vec3 velocity;

	float speed;

	float playerJumpHight = 40.0f;
	glm::vec3 jumpDirection;

	float floorHight;

	float deltaT;

	bool isGrounded;

public:
	PlayerCharacter();
	PlayerCharacter(World* world);
	~PlayerCharacter();
	void MoveForward(float value);
	void MoveRight(float value);
	void Jump();
	void Update(float deltaTime) override;

};

