#pragma once
#include "GameObject.h"
class Bullet : public GameObject
{
	glm::vec3 bulletDirection;
	float bulletSpeed;
	glm::vec3 bulletVelocity;

public:
	Bullet();
	Bullet(World* world);
	void Update(float deltaTime) override;
	void Setup(glm::vec3 direction, float speed);
	~Bullet();
};