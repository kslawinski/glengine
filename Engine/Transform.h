#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>

class Transform
{
public:
	Transform();
	~Transform();

	glm::vec3 GetPosition();
	glm::vec3 GetEulerAngleRotation();
	glm::quat GetRotationQuat();
	glm::vec3 GetScale();

	void SetEulerAnglesRotation(glm::vec3 rotation);
	void RotateYaw(float rotation);
	void RotatePitch(float rotation);
	void SetPosition(glm::vec3 position);
	void SetScale(glm::vec3 scale);

	glm::mat4 GetModel();

	glm::vec3 GetRightVector();
	glm::vec3 GetDirection();

	static glm::quat RotationBetweenVectors(glm::vec3 start, glm::vec3 dest);

	static glm::quat LookAt(glm::vec3 direction, glm::vec3 desiredUp);

	static glm::quat RotateTowards(glm::quat q1, glm::quat q2, float maxAngle);
	glm::quat rotation;
private:
	glm::vec3 position;

	glm::vec3 eulerAngles;
	glm::vec3 scale;

	float yawAngle = 3.14f;//0.0f;
						   // Initial vertical angle : none
	float pitchAngle = 0.0f;
};



