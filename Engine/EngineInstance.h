#pragma once

#define GLFW_EXPOSE_NATIVE_WGL
#define GLFW_EXPOSE_NATIVE_WIN32
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

// Include GLEW
#include <GLEW/glew.h>
// Include GLFW
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>



// Maths library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <BULLET/btBulletDynamicsCommon.h>
// Engine Includes
#include "World.h"
#include "Target.h"
#include "Pickup.h"
#include "Floor.h"
#include "Bullet.h"
#include "Platform.h"
#include "PlayerCharacter.h"
#include "UIObject.h"
#include "MeshRenderer.h"

#define GLEW_STATIC

#define SCREEN_WITH 1280//960
#define SCREEN_HIGHT 720//540

class EngineInstance
{
public:
	EngineInstance();
	EngineInstance(GLFWwindow* window);
	~EngineInstance();

	GLFWwindow* renderWindow;
	GLFWwindow* GetWindow();

	void Update(float delta);
	void Render();
	void CheckInput(float deltaTime);
	int KeyAction(int y, int action);

	World* worldInstance;
	PlayerCharacter* player;
	// Render area variables
	int renderWith = SCREEN_WITH;
	int renderHight = SCREEN_HIGHT;

	bool RenderWindowShouldClose = false;

	// BULLET TEMP VARS
	GameObject* cube;
	btCollisionShape* fallShape;
	btRigidBody* fallRigidBody;

	// TEMP UI VARS TODO implement list of UI objects in World
	UIObject* uiObject;
	int SpriteAnimindex = 0;
	// For speed computation
	double lastTime2;
	int lastcos = 0;
	int nbFrames = 0;
};

