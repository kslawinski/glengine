#define GLFW_EXPOSE_NATIVE_WGL
#define GLFW_EXPOSE_NATIVE_WIN32
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include "EngineInstance.h"

int main()
{
	EngineInstance*  engineInstance = new EngineInstance();
	
	// For speed computation
	double lastTime2 = glfwGetTime();
	int nbFrames = 0;

	float deltaTime = 0;
	int lastcos;
	int index = 0;

	do 
	{
		// glfwGetTime is called only once, the first time this function is called
		static double lastTime = glfwGetTime();
		// Measure speed
		// Compute time difference between current and last frame
		double currentTime = glfwGetTime();
		deltaTime = float(currentTime - lastTime);
		nbFrames++;

		engineInstance->Render();
		engineInstance->Update(deltaTime);

		// For the next frame, the "last time" will be "now"
		lastTime = currentTime;
	} while (engineInstance->RenderWindowShouldClose == false);
	
	delete engineInstance;
	return 1;

	/*

	//glm::vec2 outScreenPos;
	World* worldInstance;

	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // 4.0 zamiast 4.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(SCREEN_WITH, SCREEN_HIGHT, "OPEN GL 3D SCENE", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 4.3 compatible.\n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Ensure we can capture the escape key being pressed
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	//glClearColor(0.2f, 0.1f, 0.0f, 0.0f); //set background colour (uncomment if required)
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f); //set background colour (uncomment if required)

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	//PROGRAM STARTS HERE.................................................................................................

	worldInstance = new World();

	worldInstance->Instantiate<Floor>();
	worldInstance->Instantiate<Target>(glm::vec3(0.0f, 0.7f, -4.0f));
	worldInstance->Instantiate<Pickup>(glm::vec3(-3.0f, 0.0f, 3.0f));
	worldInstance->Instantiate<Pickup>(glm::vec3(-2.3f, 0.0f, 1.5f));
	worldInstance->Instantiate<Pickup>(glm::vec3(-4.0f, 0.0f, -2.0f));
	worldInstance->Instantiate<Platform>(glm::vec3(3.0f, 1.0f, 2.0f));
	GameObject* cube = worldInstance->Instantiate<GameObject>(glm::vec3(-3.0f, 0.0f, 2.0f));

	PlayerCharacter* playerCharacter = worldInstance->Instantiate<PlayerCharacter>(glm::vec3(0.0f, 0.0f, 5.0f));

	UIObject* uiObject = new UIObject();

	MeshRenderer* rend = new MeshRenderer(worldInstance->mainCamera);

	// For speed computation
	double lastTime2 = glfwGetTime();
	int nbFrames = 0;

	// BULLET SHAPE FOR SIMULATION IN WORLD

	btCollisionShape* fallShape = new btBoxShape(btVector3(1, 1, 1));//new btSphereShape(1);

	btDefaultMotionState* fallMotionState =
		new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 50, 0)));
	btScalar mass = 1;
	btVector3 fallInertia(0, 0, 0);
	fallShape->calculateLocalInertia(mass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);
	btRigidBody* fallRigidBody = new btRigidBody(fallRigidBodyCI);
	worldInstance->dynamicsWorld->addRigidBody(fallRigidBody);

	do {

		// BULLET SHAPE Movement
		btTransform trans;
		fallRigidBody->getMotionState()->getWorldTransform(trans);

		std::cout << "sphere height: " << trans.getOrigin().getY() << std::endl;

		cube->objTransform->SetPosition(glm::vec3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
		cube->objTransform->rotation = glm::quat(trans.getRotation().getX(), trans.getRotation().getY(), trans.getRotation().getZ(), trans.getRotation().getW());

		// glfwGetTime is called only once, the first time this function is called
		static double lastTime = glfwGetTime();
		//
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 	// Clear the screen
		for (GameObject* obj : worldInstance->worldObjects) // render all instances
		{
			obj->Draw();
		}
		std::string str;// = "FPS: ";

						// Measure speed
		double currentTime2 = glfwGetTime();
		nbFrames++;
		if (currentTime2 - lastTime2 >= 0.15f) { // If last prinf() was more than 1sec ago
			lastcos = double(nbFrames);
			nbFrames = 0;
			lastTime2 += 0.15;							 // printf and reset
			index++;
		}

		if (uiObject != nullptr)
		{
			str.append(std::to_string(worldInstance->playerPoints));
			uiObject->printText2D(&str[0], 55, 520, 45);
		}

		uiObject->printSprite(index, 10, 520, 45); //10,520
		worldInstance->Update(deltaTime); // update world

										  // Measure speed
										  // Compute time difference between current and last frame
		double currentTime = glfwGetTime();
		deltaTime = float(currentTime - lastTime);
		nbFrames++;



		glfwSwapBuffers(window);		//display
		glfwPollEvents();				//check for key presses etc.

		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)) {

			// Get mouse position
			double xpos, ypos;
			glfwGetCursorPos(window, &xpos, &ypos);

			ypos = SCREEN_HIGHT - ypos; // invert ypos

			xpos = 10;
			ypos = 520;

			glm::vec3 ray_origin;
			glm::vec3 ray_direction;
			worldInstance->ScreenPosToWorldRay(
				xpos, ypos,
				SCREEN_WITH, SCREEN_HIGHT,
				//ViewMatrix,
				//ProjectionMatrix,
				ray_origin,
				ray_direction
			);
			std::cout << "MouseY: " << ypos << std::endl;
			//Bullet* bullet = worldInstance->Instantiate<Bullet>(ray_origin + (ray_direction * 10.0f));
			Bullet* bullet = worldInstance->Instantiate<Bullet>();
			bullet->Setup(glm::normalize((ray_origin + (ray_direction)) - bullet->objTransform->GetPosition()), 20);

			/*
			outScreenPos;
			glm::vec3 in3Dpos = glm::vec3(0.0f, 0.0f, 0.0f);

			worldInstance->WorldToScreenPos(xpos, ypos, 960, 540, outScreenPos,in3Dpos);

			std::cout << "outScreenPosX: " << outScreenPos.x << std::endl;
			*/

/*
		}

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) // When D keyboard is pressed
		{
			//camera.RotateCameraY((-glfwGetTime()));
			worldInstance->mainCamera->RotateCameraY(deltaTime * 5);
			//view = camera.GetViewMatrix();
		}

		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		{
			//camera.RotateCameraY((glfwGetTime()));
			worldInstance->mainCamera->RotateCameraY(-deltaTime * 5);
			//view = camera.GetViewMatrix();
		}

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		{
			worldInstance->mainCamera->RotateCameraX(-deltaTime * 2);
			//view = camera.GetViewMatrix();
		}
		static int oldState = GLFW_RELEASE;
		int state = glfwGetKey(window, GLFW_KEY_SPACE);
		if (state == GLFW_PRESS && oldState == GLFW_RELEASE)
		{
			playerCharacter->Jump();
		}
		oldState = state;

		static int oldState2 = GLFW_RELEASE;
		int state2 = glfwGetKey(window, GLFW_KEY_ENTER);
		if (state2 == GLFW_PRESS && oldState2 == GLFW_RELEASE)
		{
			Bullet* bullet = worldInstance->Instantiate<Bullet>(playerCharacter->objTransform->GetPosition() + playerCharacter->objTransform->GetDirection() + glm::vec3(0.1f, 0.8f, 0.1f));
			bullet->Setup(playerCharacter->objTransform->GetDirection(), 20);
		}

		oldState2 = state2;


		static int gOldState = GLFW_RELEASE;
		gOldState = KeyAction(GLFW_KEY_G, gOldState);

		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		{
			worldInstance->mainCamera->RotateCameraX(deltaTime * 2);
			//view = camera.GetViewMatrix();
		}

		if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
		{
			playerCharacter->MoveForward(deltaTime * 8.0f); // move right
		}
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
		{
			//worldInstance->Instantiate<Pickup>();
			playerCharacter->objTransform->RotateYaw(deltaTime * 5.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
		{
			//worldInstance->Instantiate<Pickup>();
			playerCharacter->objTransform->RotateYaw(deltaTime * -5.0f);
		}

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		{
			//worldInstance->mainCamera->MoveForward(8.0f * deltaTime);
			playerCharacter->MoveForward(deltaTime * 8.0f); // move player forward
															//playerCharacter->MoveForward(8.0f * deltaTime);
															//view = camera.GetViewMatrix();
		}

		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		{
			//worldInstance->mainCamera->MoveForward(-8.0f * deltaTime);
			//playerCharacter->MoveForward(-8.0f * deltaTime);
			//view = camera.GetViewMatrix();
		}

		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		{
			//worldInstance->mainCamera->MoveRight(-deltaTime * 8.0f);
			playerCharacter->objTransform->RotateYaw(deltaTime * 5.0f);
			//playerCharacter->MoveRight(-deltaTime * 8.0f); // move left
		}

		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		{
			//worldInstance->mainCamera->MoveRight(deltaTime * 8.0f);
			playerCharacter->objTransform->RotateYaw(deltaTime * -5.0f);
			//playerCharacter->MoveRight(deltaTime * 8.0f); // move right
			//std::cout << "player position is: " << playerCharacter->objTransform->GetPosition().x << std::endl;
		}

		// For the next frame, the "last time" will be "now"
		lastTime = currentTime;

	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0);

	// Close GUI and OpenGL window, and terminate GLFW
	//TwTerminate();
	//glfwTerminate();



	//worldInstance->dynamicsWorld->removeRigidBody(fallRigidBody);
	delete fallRigidBody->getMotionState();
	delete fallRigidBody;




	delete fallShape;



	delete worldInstance;


	//..................................................................................................................
	return 0;

	*/
}