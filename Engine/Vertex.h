#pragma once
#include <glm/glm.hpp>

class Vertex // vertex class to store information about vertex position and colour
{
public:
	glm::vec3 position; // declare variables that each object of this type will have
	glm::vec3 colour;
	glm::vec2 uvCord;

	Vertex();

	~Vertex();

	Vertex(glm::vec3 position, glm::vec3 colour, glm::vec2 uvCord);
};

