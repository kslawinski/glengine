#include "EngineInstance.h"


#define GLEW_STATIC


EngineInstance::EngineInstance()
{
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		//return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // 4.0 zamiast 4.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

	// Open a window and create its OpenGL context
	renderWindow = glfwCreateWindow(SCREEN_WITH, SCREEN_HIGHT, "OPEN GL 3D SCENE", NULL, NULL);
	if (renderWindow == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 4.3 compatible.\n");
		glfwTerminate();
		//return -1;
	}
	glfwMakeContextCurrent(renderWindow);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		//return -1;
	}

	// Ensure we can capture the escape key being pressed
	glfwSetInputMode(renderWindow, GLFW_STICKY_KEYS, GL_TRUE);

	glClearColor(0.0f, 0.1f, 0.4f, 0.0f); //set background colour (uncomment if required)
	//glClearColor(1.0f, 0.0f, 1.0f, 0.0f); //set background colour (uncomment if required)

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glfwShowWindow(renderWindow);

	worldInstance = new World(); // this stores objects on map and could be used as Current level info TODO : make save map to file

	// SPAWN SOME OBJECTS IN THE WORLD
	player = worldInstance->Instantiate<PlayerCharacter>(glm::vec3(0.0f, 0.0f, 5.0f));
	worldInstance->Instantiate<Floor>();
	worldInstance->Instantiate<Target>(glm::vec3(0.0f, 0.7f, -4.0f));
	worldInstance->Instantiate<Pickup>(glm::vec3(-3.0f, 0.0f, 3.0f));
	worldInstance->Instantiate<Pickup>(glm::vec3(-2.3f, 0.0f, 1.5f));
	worldInstance->Instantiate<Pickup>(glm::vec3(-4.0f, 0.0f, -2.0f));
	worldInstance->Instantiate<Platform>(glm::vec3(3.0f, 1.0f, 2.0f));
	cube = worldInstance->Instantiate<GameObject>(glm::vec3(-3.0f, 0.0f, 2.0f));

	uiObject = new UIObject(); // create ui
	lastTime2 = glfwGetTime();

	// BULLET SHAPE FOR SIMULATION IN WORLD
	fallShape = new btBoxShape(btVector3(1, 1, 1));//new btSphereShape(1);
	btDefaultMotionState* fallMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 50, 0)));
	btScalar mass = 1;
	btVector3 fallInertia(0, 0, 0);
	fallShape->calculateLocalInertia(mass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);
	fallRigidBody = new btRigidBody(fallRigidBodyCI);
	worldInstance->dynamicsWorld->addRigidBody(fallRigidBody);
}

EngineInstance::EngineInstance(GLFWwindow * window) // this is to be used with Editor in future
{
}


EngineInstance::~EngineInstance()
{
	glfwTerminate();
	worldInstance->dynamicsWorld->removeRigidBody(fallRigidBody);
	delete fallRigidBody->getMotionState();
	delete fallRigidBody;
	delete fallShape;
	delete worldInstance;
}

GLFWwindow * EngineInstance::GetWindow()
{
	return renderWindow;
}

void EngineInstance::Update(float delta)
{
	CheckInput(delta);

	worldInstance->Update(delta);

	// BULLET SHAPE Movement
	btTransform trans;
	fallRigidBody->getMotionState()->getWorldTransform(trans);

	std::cout << "cube height: " << trans.getOrigin().getY() << std::endl;

	cube->objTransform->SetPosition(glm::vec3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
	cube->objTransform->rotation = glm::quat(trans.getRotation().getX(), trans.getRotation().getY(), trans.getRotation().getZ(), trans.getRotation().getW());

}

void EngineInstance::Render()
{
	if (glfwGetKey(renderWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(renderWindow) == 0)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 	// Clear the screen
		if (worldInstance == nullptr) { return; }
		if(worldInstance->worldObjects.size() > 0)
		for (GameObject* obj : worldInstance->worldObjects) // render all instances
		{
			obj->Draw();
		}

		// Render UI
		if (uiObject != nullptr)
		{
			std::string str;// = "FPS: ";
			str.append(std::to_string(worldInstance->playerPoints));
			uiObject->printText2D(&str[0], 55, 520, 45);
		}

		double currentTime2 = glfwGetTime();
		nbFrames++;
		if (currentTime2 - lastTime2 >= 0.15f) { // If last prinf() was more than 1sec ago
			lastcos = double(nbFrames);
			nbFrames = 0;
			lastTime2 += 0.15;							 // printf and reset
			SpriteAnimindex++;
		}
		uiObject->printSprite(SpriteAnimindex, 10, 520, 45); //10,520

		glfwSwapBuffers(renderWindow);
		glfwPollEvents();
	}
	else
	{
		RenderWindowShouldClose = true;
		glfwTerminate();
		return;
	}
}

void EngineInstance::CheckInput(float deltaTime)
{
	if (glfwGetKey(renderWindow, GLFW_KEY_RIGHT) == GLFW_PRESS) // When D keyboard is pressed
	{
		//camera.RotateCameraY((-glfwGetTime()));
		worldInstance->mainCamera->RotateCameraY(deltaTime * 5);
		//view = camera.GetViewMatrix();
	}

	if (glfwGetKey(renderWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		//camera.RotateCameraY((glfwGetTime()));
		worldInstance->mainCamera->RotateCameraY(-deltaTime * 5);
		//view = camera.GetViewMatrix();
	}

	if (glfwGetKey(renderWindow, GLFW_KEY_UP) == GLFW_PRESS)
	{
		worldInstance->mainCamera->RotateCameraX(-deltaTime * 2);
		//view = camera.GetViewMatrix();
	}
	static int oldState = GLFW_RELEASE;
	int state = glfwGetKey(renderWindow, GLFW_KEY_SPACE);
	if (state == GLFW_PRESS && oldState == GLFW_RELEASE)
	{
		player->Jump();
	}
	oldState = state;

	static int oldState2 = GLFW_RELEASE;
	int state2 = glfwGetKey(renderWindow, GLFW_KEY_ENTER);
	if (state2 == GLFW_PRESS && oldState2 == GLFW_RELEASE)
	{
		Bullet* bullet = worldInstance->Instantiate<Bullet>(player->objTransform->GetPosition() + player->objTransform->GetDirection() + glm::vec3(0.1f, 0.8f, 0.1f));
		bullet->Setup(player->objTransform->GetDirection(), 20);
	}

	oldState2 = state2;

	static int gOldState = GLFW_RELEASE;
	gOldState = KeyAction(GLFW_KEY_G, gOldState);

	if (glfwGetKey(renderWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		worldInstance->mainCamera->RotateCameraX(deltaTime * 2);
		//view = camera.GetViewMatrix();
	}

	if (glfwGetKey(renderWindow, GLFW_KEY_F) == GLFW_PRESS)
	{
		player->MoveForward(deltaTime * 8.0f); // move right
	}
	if (glfwGetKey(renderWindow, GLFW_KEY_Q) == GLFW_PRESS)
	{
		///worldInstance->Instantiate<Pickup>();
		player->objTransform->RotateYaw(deltaTime * 5.0f);
	}
	if (glfwGetKey(renderWindow, GLFW_KEY_E) == GLFW_PRESS)
	{
		///worldInstance->Instantiate<Pickup>();
		player->objTransform->RotateYaw(deltaTime * -5.0f);
	}

	if (glfwGetKey(renderWindow, GLFW_KEY_W) == GLFW_PRESS)
	{
		///worldInstance->mainCamera->MoveForward(8.0f * deltaTime);
		player->MoveForward(deltaTime * 8.0f); // move player forward
														//player->MoveForward(8.0f * deltaTime);
														//view = camera.GetViewMatrix();
	}

	if (glfwGetKey(renderWindow, GLFW_KEY_S) == GLFW_PRESS)
	{
		///worldInstance->mainCamera->MoveForward(-8.0f * deltaTime);
		player->MoveForward(-8.0f * deltaTime);
		///view = camera.GetViewMatrix();
	}

	if (glfwGetKey(renderWindow, GLFW_KEY_A) == GLFW_PRESS)
	{
		///worldInstance->mainCamera->MoveRight(-deltaTime * 8.0f);
		player->objTransform->RotateYaw(deltaTime * 5.0f);
		///player->MoveRight(-deltaTime * 8.0f); // move left
	}

	if (glfwGetKey(renderWindow, GLFW_KEY_D) == GLFW_PRESS)
	{
		//worldInstance->mainCamera->MoveRight(deltaTime * 8.0f);
		player->objTransform->RotateYaw(deltaTime * -5.0f);
		//player->MoveRight(deltaTime * 8.0f); // move right
		//std::cout << "player position is: " << player->objTransform->GetPosition().x << std::endl;
	}
}

int EngineInstance::KeyAction(int key, int lastState)
{
	int state3 = glfwGetKey(renderWindow, key);

	switch (key)
	{
	case GLFW_KEY_G:
	{
		if (state3 == GLFW_PRESS && lastState == GLFW_RELEASE)
		{
			std::cout << "G key pressed" << std::endl;
		}
		break;
	}
	default:
		break;
	}
	return state3;
}
