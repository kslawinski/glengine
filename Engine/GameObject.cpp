#include "GameObject.h"
#include "Vertex.h"
#include "Material.h"
#include "Camera.h"
#include "World.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



GameObject::GameObject(World* world)
{

	Init(*world->mainCamera);
	worldInstance = world;

	Vertex CubeData[36] = { // Create Vertex data objects storing data about vertex positions and colours to be used to create cube object
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.000103f, 1.0f - 0.336048f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(1.000023f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.999958f, 1.0f - 0.336064f)),

		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(0.0f,1.0f,0.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667969f, 1.0f - 0.671889f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(1.000023f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.668104f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),

		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.336098f, 1.0f - 0.000071f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(-1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),

		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(1.000004f, 1.0f - 0.671847f)),
		Vertex(glm::vec3(-1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.999958f, 1.0f - 0.336064f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.668104f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),

		Vertex(glm::vec3(1.0f,-1.0f,-1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.668104f, 1.0f - 0.000013f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.336098f, 1.0f - 0.000071f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000103f, 1.0f - 0.336048f)),
		Vertex(glm::vec3(1.0f, 1.0f,-1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.000004f, 1.0f - 0.671870f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(0.0f,0.0f,1.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),

		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.000103f, 1.0f - 0.336048f)),
		Vertex(glm::vec3(-1.0f, 1.0f,-1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.336024f, 1.0f - 0.671877f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.335973f, 1.0f - 0.335903f)),
		Vertex(glm::vec3(1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667969f, 1.0f - 0.671889f)),
		Vertex(glm::vec3(-1.0f, 1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(1.000004f, 1.0f - 0.671847f)),
		Vertex(glm::vec3(1.0f,-1.0f, 1.0f),glm::vec3(1.0f,0.0f,0.0f),glm::vec2(0.667979f, 1.0f - 0.335851f)),

	};
	objMesh = new Mesh(CubeData, 36); // create Cube using data stored in Vertex objects

	objectTag = "Game Object";

	//objTexture = new Texture("./bricks.jpg");
	//objTexture->Bind();



	//myTexture = loadDDS("uvtemplate.DDS");
	//myTexture = //loadBMP_custom("uvtemplate.bmp");

	//if (myTexture == NULL) 
	//{
	//	printf("NIE MA TEXTURY!!");
	//}
}

GameObject::GameObject()
{
	//objectTag = "Game Object";
}

GameObject::~GameObject()
{
	std::cout << "Object Destroyed!" << std::endl;
	delete objShader;
	delete objMesh;
	//delete objTexture;
	delete objTransform;
	//	glDeleteTextures(1, &myTexture);
}

void GameObject::Update(float deltaTime)
{

}

void GameObject::Draw()
{
	//objTransform->SetPosition(glm::vec3(objTransform->GetPosition().x + 0.01f, 0.0f,0.0f));

	//objTexture->Bind();
	objMaterial->materialShader->Bind();

	view = camera->GetViewMatrix();
	model = objTransform->GetModel();

	if (objMaterial == nullptr) { return; }

	GLuint MVid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "ModelView");		//get handle
	glUniformMatrix4fv(MVid, 1, GL_FALSE, &modelView[0][0]);			//link it to mvp matrix

	GLuint Normid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Norm");		//get handl
	glUniformMatrix4fv(Normid, 1, GL_FALSE, &normMatrix[0][0]);			//link it to mvp matrix

	GLuint MVPid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "MVP");		//get handle
	glUniformMatrix4fv(MVPid, 1, GL_FALSE, &MVP[0][0]);			//link it to mvp matrix

																// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, objMaterial->materialTexture);
	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "myTextureSampler");
	// Set our "myTextureSampler" sampler to use Texture Unit 0
	glUniform1i(TextureID, 0);

	MVP = proj * view*model; // refresh the transformations
							 //objMaterial->materialShader->Bind();
	if (objMesh != nullptr)
	{
		//
		//if (objTexture != nullptr)
			//
		//{
			//objTexture->Bind();
			//
		//}
		objMesh->Draw();
	}
}


void GameObject::Init(Camera& camera)
{
	objMaterial = new Material();
	//printf("Camera INIT!!");
	objMaterial->materialShader->Bind();
	this->camera = &camera;

	objTransform = new Transform();

	model = objTransform->GetModel();

	view = this->camera->GetViewMatrix();
	proj = this->camera->GetProjectionMatrix();

	//multiply matrices: to calculate objects transformations
	MVP = proj * view * model;
	modelView = view * model;

	normMatrix = glm::inverseTranspose(glm::mat3(modelView));

	//objShader = objMaterial->materialShader;//= new Shader();

	// Build lighting
	glm::vec3 LPos(-1, -1, 1);
	glm::vec3 Ka(0.3, 0.3, 0.3); // Ambient reflectivity
	glm::vec3 Kd(0.5, 0.5, 0.5); // Diffuse Reflectivity
	glm::vec3 Ks(0.7, 0.7, 0.7); // SpecularReflectivity
	glm::vec3 Ld(1, 1, 1);

	//objShader->Bind(); // use shader

	GLuint LPid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "LightPosition");		//get handle
	glUniform3fv(LPid, 1, &LPos[0]);			//link it to mvp matrix

	GLuint Kdid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Ka");		//get handle
	glUniform3fv(Kdid, 1, &Kd[0]);			//link it to mvp matrix

	GLuint Ksid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Kd");		//get handle
	glUniform3fv(Ksid, 1, &Ks[0]);			//link it to mvp matrix

	GLuint Kaid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Ks");		//get handle
	glUniform3fv(Kaid, 1, &Ka[0]);			//link it to mvp matrix

	GLuint Ldid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Ld");		//get handle
	glUniform3fv(Ldid, 1, &Ld[0]);			//link it to mvp matrix


											// Get a handle for our "myTextureSampler" uniform
											//GLuint TextureID = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "myTextureSampler");
}

glm::mat4 GameObject::GetModelMatrix()
{
	return model;
}

void GameObject::SetMaterial(Material * mat)
{
	delete objMaterial;
	objMaterial = mat;
	objMaterial->materialShader->Bind();


	// Build lighting
	glm::vec3 LPos(-1, -1, 1);
	glm::vec3 Ka(0.3, 0.3, 0.3); // Ambient reflectivity
	glm::vec3 Kd(0.5, 0.5, 0.5); // Diffuse Reflectivity
	glm::vec3 Ks(0.7, 0.7, 0.7); // SpecularReflectivity
	glm::vec3 Ld(1, 1, 1);

	//objShader->Bind(); // use shader

	GLuint LPid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "LightPosition");		//get handle
	glUniform3fv(LPid, 1, &LPos[0]);			//link it to mvp matrix

	GLuint Kdid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Ka");		//get handle
	glUniform3fv(Kdid, 1, &Kd[0]);			//link it to mvp matrix

	GLuint Ksid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Kd");		//get handle
	glUniform3fv(Ksid, 1, &Ks[0]);			//link it to mvp matrix

	GLuint Kaid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Ks");		//get handle
	glUniform3fv(Kaid, 1, &Ka[0]);			//link it to mvp matrix

	GLuint Ldid = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "Ld");		//get handle
	glUniform3fv(Ldid, 1, &Ld[0]);			//link it to mvp matrix


											// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID = glGetUniformLocation(objMaterial->materialShader->shaderProgram, "myTextureSampler");

	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, objMaterial->materialTexture);
	// Set our "myTextureSampler" sampler to use Texture Unit 0
	//glUniform1i(TextureID, 0);
}

int GameObject::CheckCollisionWith(GameObject& other)
{
	int collisioFlag = 0;

	if (&other == nullptr) { return 0; }

	//std::cout << GetPosition().y << " "<< other.GetPosition().y << std::endl;

	//if (GetShape().top <= other.GetPosition().y &&  GetShape().top - GetShape().height >= other.GetPosition().y - other.GetShape().height)
	float otherBottom, otherTop, pBottom, pTop;
	float otherLeft, otherRight, pLeft, pRight;
	float otherFront, pFront, otherBack, pBack;
	float topOffset = 3;
	float sideOffset = 0.5f;

	otherLeft = (other.objTransform->GetPosition().x + other.objMesh->meshBounds->leftBound);
	otherRight = (other.objTransform->GetPosition().x + other.objMesh->meshBounds->rightBound);

	otherFront = (other.objTransform->GetPosition().z + other.objMesh->meshBounds->frontBound);
	otherBack = (other.objTransform->GetPosition().z + other.objMesh->meshBounds->backBound);

	otherTop = (other.objTransform->GetPosition().y + other.objMesh->meshBounds->topBound);
	otherBottom = (other.objTransform->GetPosition().y + other.objMesh->meshBounds->bottomBound);

	pFront = (this->objTransform->GetPosition().z + this->objMesh->meshBounds->frontBound);
	pBack = (this->objTransform->GetPosition().z + this->objMesh->meshBounds->backBound);

	pLeft = (this->objTransform->GetPosition().x + this->objMesh->meshBounds->leftBound);
	pRight = (this->objTransform->GetPosition().x + this->objMesh->meshBounds->rightBound);

	pTop = (this->objTransform->GetPosition().y + this->objMesh->meshBounds->topBound);
	pBottom = (this->objTransform->GetPosition().y + this->objMesh->meshBounds->bottomBound);

	if ((pLeft >= otherLeft && pRight <= otherRight) && (pFront <= otherFront && pBack >= otherBack))
	{
		collisioFlag = 1;
		//std::cout << "Collision flag: " << collisioFlag << std::endl;
	}
	if ((pBottom > otherBottom) && (pLeft >= otherLeft && pRight <= otherRight) && (pFront <= otherFront && pBack >= otherBack))
	{
		collisioFlag = 2;
	}
	//TODO IMPLEMENT collision from left and right
	if ((pLeft >= otherLeft && pRight <= otherRight) && (pFront >= otherFront && pBack >= otherBack))
	{
		collisioFlag = 3;
		//std::cout << "Collision flag: " << collisioFlag << std::endl;
	}

	return collisioFlag;
}

GLuint GameObject::loadBMP_custom(const char * imagepath) {

	printf("Reading image %s\n", imagepath);

	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = fopen(imagepath, "rb");
	if (!file) {
		printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath);
		getchar();
		return 0;
	}

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54) {
		printf("Not a correct BMP file\n");
		fclose(file);
		return 0;
	}
	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
		fclose(file);
		return 0;
	}
	// Make sure this is a 24bpp file
	if (*(int*)&(header[0x1E]) != 0) { printf("Not a correct BMP file\n");    fclose(file); return 0; }
	if (*(int*)&(header[0x1C]) != 24) { printf("Not a correct BMP file\n");    fclose(file); return 0; }

	// Read the information about the image
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)    imageSize = width * height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way

										 // Create a buffer
	data = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	// Everything is in memory now, the file can be closed.
	fclose(file);

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	// OpenGL has now copied the data. Free our own version
	delete[] data;

	// Poor filtering, or ...
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 

	// ... nice trilinear filtering ...
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	// ... which requires mipmaps. Generate them automatically.
	glGenerateMipmap(GL_TEXTURE_2D);

	// Return the ID of the texture we just created
	return textureID;
}

bool GameObject::loadOBJ(const char * path, std::vector<glm::vec3>& out_vertices, std::vector<glm::vec2>& out_uvs, std::vector<glm::vec3>& out_normals)
{
	printf("Loading OBJ file %s...\n", path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;

	FILE * file = fopen(path, "r");
	if (file == NULL) {
		printf("Impossible to open the file ! Are you in the right path ? See Tutorial 1 for details\n");
		getchar();
		return false;
	}

	while (1) {

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

				   // else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				fclose(file);
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else {
			// Probably a comment, eat up the rest of the line
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i<vertexIndices.size(); i++) {

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);

	}
	fclose(file);
	return true;
}

bool GameObject::TestRayOBBIntersection(
	glm::vec3 ray_origin,        // Ray origin, in world space
	glm::vec3 ray_direction,     // Ray direction (NOT target position!), in world space. Must be normalize()'d.
	glm::vec3 aabb_min,          // Minimum X,Y,Z coords of the mesh when not transformed at all.
	glm::vec3 aabb_max,          // Maximum X,Y,Z coords. Often aabb_min*-1 if your mesh is centered, but it's not always the case.
	glm::mat4 ModelMatrix,       // Transformation applied to the mesh (which will thus be also applied to its bounding box)
	float& intersection_distance // Output : distance between ray_origin and the intersection with the OBB
) {

	// Intersection method from Real-Time Rendering and Essential Mathematics for Games

	float tMin = 0.0f;
	float tMax = 100000.0f;

	glm::vec3 OBBposition_worldspace(ModelMatrix[3].x, ModelMatrix[3].y, ModelMatrix[3].z);

	glm::vec3 delta = OBBposition_worldspace - ray_origin;

	// Test intersection with the 2 planes perpendicular to the OBB's X axis
	{
		glm::vec3 xaxis(ModelMatrix[0].x, ModelMatrix[0].y, ModelMatrix[0].z);
		float e = glm::dot(xaxis, delta);
		float f = glm::dot(ray_direction, xaxis);

		if (fabs(f) > 0.001f) { // Standard case

			float t1 = (e + aabb_min.x) / f; // Intersection with the "left" plane
			float t2 = (e + aabb_max.x) / f; // Intersection with the "right" plane
											 // t1 and t2 now contain distances betwen ray origin and ray-plane intersections

											 // We want t1 to represent the nearest intersection, 
											 // so if it's not the case, invert t1 and t2
			if (t1>t2) {
				float w = t1; t1 = t2; t2 = w; // swap t1 and t2
			}

			// tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
			if (t2 < tMax)
				tMax = t2;
			// tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
			if (t1 > tMin)
				tMin = t1;

			// And here's the trick :
			// If "far" is closer than "near", then there is NO intersection.
			// See the images in the tutorials for the visual explanation.
			if (tMax < tMin)
				return false;

		}
		else { // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
			if (-e + aabb_min.x > 0.0f || -e + aabb_max.x < 0.0f)
				return false;
		}
	}


	// Test intersection with the 2 planes perpendicular to the OBB's Y axis
	// Exactly the same thing than above.
	{
		glm::vec3 yaxis(ModelMatrix[1].x, ModelMatrix[1].y, ModelMatrix[1].z);
		float e = glm::dot(yaxis, delta);
		float f = glm::dot(ray_direction, yaxis);

		if (fabs(f) > 0.001f) {

			float t1 = (e + aabb_min.y) / f;
			float t2 = (e + aabb_max.y) / f;

			if (t1>t2) { float w = t1; t1 = t2; t2 = w; }

			if (t2 < tMax)
				tMax = t2;
			if (t1 > tMin)
				tMin = t1;
			if (tMin > tMax)
				return false;

		}
		else {
			if (-e + aabb_min.y > 0.0f || -e + aabb_max.y < 0.0f)
				return false;
		}
	}


	// Test intersection with the 2 planes perpendicular to the OBB's Z axis
	// Exactly the same thing than above.
	{
		glm::vec3 zaxis(ModelMatrix[2].x, ModelMatrix[2].y, ModelMatrix[2].z);
		float e = glm::dot(zaxis, delta);
		float f = glm::dot(ray_direction, zaxis);

		if (fabs(f) > 0.001f) {

			float t1 = (e + aabb_min.z) / f;
			float t2 = (e + aabb_max.z) / f;

			if (t1>t2) { float w = t1; t1 = t2; t2 = w; }

			if (t2 < tMax)
				tMax = t2;
			if (t1 > tMin)
				tMin = t1;
			if (tMin > tMax)
				return false;

		}
		else {
			if (-e + aabb_min.z > 0.0f || -e + aabb_max.z < 0.0f)
				return false;
		}
	}

	intersection_distance = tMin;
	return true;

}