#version 430

in vec3 LightIntensity;
// Interpolated values from the vertex shaders
in vec2 UV;

//out vec4 colour;
out vec3 color;

// Values that stay constant for the whole mesh.
uniform sampler2D myTextureSampler;



void main()
{
    //colour = vec4(LightIntensity,1);
	color = texture( myTextureSampler, UV.xy).rgb * (LightIntensity * 1.4f);
	//color = texture( myTextureSampler, vec2(0.2,0.2)).rgb;
	//color = vec3 (1,0,0);
	//gl_FragColor = vec4(1,1,0,1);
}


/**void main()
{
    gl_FragColor = vec4(1,1,0,1);
}*/
