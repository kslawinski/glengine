#include "PlayerCharacter.h"
#include "Camera.h"
#include "World.h"
#include "Platform.h"
#include "Pickup.h"
#include "Material.h"

PlayerCharacter::PlayerCharacter()
{
}

PlayerCharacter::PlayerCharacter(World* world)
{
	Init(*world->mainCamera);
	worldInstance = world;
	//myTexture = loadBMP_custom("uvtemplatemy.bmp");

	Vertex CubeData[36] = { // Create Vertex data objects storing data about vertex positions and colours to be used to create cube object
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f,-0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.2f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.3f)),
		Vertex(glm::vec3(0.4f, 0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.1f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f, 0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f, 0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),

		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f,-0.4f,-0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f,-0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.4f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f, 0.4f,-0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f, 0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(-0.4f, 0.4f, 0.4f),glm::vec3(0.0f,0.0f,0.4f),glm::vec2(0.000059f, 1.0f - 0.000004f)),
		Vertex(glm::vec3(0.4f,-0.4f, 0.4f),glm::vec3(0.4f,0.0f,0.0f),glm::vec2(0.000059f, 1.0f - 0.000004f)),

	};

	objMesh = new Mesh(CubeData, 36); // create Cube using data stored in Vertex objects
	objTransform->SetScale(glm::vec3(1.0f, 2.0f, 1.0f));
	//objTransform->SetPosition(glm::vec3(0.0f,-0.4f, 0.0f));

	SetMaterial(new Material("EngineContent/Textures/uvtemplatemy.bmp"));

	//SET CAMERA POSITION
	world->mainCamera->MoveUp(2.1f);
	world->mainCamera->MoveForward(-6.1f);

	floorHight = objTransform->GetPosition().y;

	objectTag = "Player";

	//std::cout << "PLAYER BOTTOM! : " << objMesh->meshBounds->bottomBound << "FLOOR HEIGht: " << floorHight;
}


PlayerCharacter::~PlayerCharacter()
{
	//GameObject::~GameObject();
}

void PlayerCharacter::MoveForward(float value)
{
	//objTransform->SetPosition(objTransform->GetPosition() -= glm::vec3(0, 0, value));
	objTransform->SetPosition(objTransform->GetPosition() += objTransform->GetDirection() * value);
}

void PlayerCharacter::MoveRight(float value)
{
	//objTransform->SetPosition(objTransform->GetPosition() += glm::vec3(value, 0, 0));
	objTransform->SetPosition(objTransform->GetPosition() += objTransform->GetRightVector() * value);
}

void PlayerCharacter::Jump()
{
	if (isGrounded) {
		velocity.y -= playerJumpHight;
		isGrounded = false;
	}

	//Raycast Test

	for (int i = 0; i < worldInstance->worldObjects.size(); i++)
	{
		float intersectionDistance;

		if (worldInstance->worldObjects[i]->TestRayOBBIntersection(this->objTransform->GetPosition() + glm::vec3(0, 0.4f, 0.5f), glm::normalize(this->objTransform->GetDirection()), glm::vec3(-1, -1, -1), glm::vec3(1, 1, 1), worldInstance->worldObjects[i]->GetModelMatrix(), intersectionDistance))
		{
			if (worldInstance->worldObjects[i]->objectTag == "Environment") { continue; }

			if (worldInstance->worldObjects[i] != this)
				std::cout << "RAYCAST WORK " << worldInstance->worldObjects[i]->objectTag << "Distance: " << intersectionDistance << std::endl;
		}
	}
}

void PlayerCharacter::Update(float deltaTime)
{
	GameObject::Update(deltaTime);
	deltaT = deltaTime;

	if (objMesh)
	{
		//std::cout << "Right collision: " << objMesh->meshBounds->leftBound << std::endl;
	}

	if (worldInstance == nullptr) { return; }
	float distance;
	GameObject* closestObject = worldInstance->GetClosestObject(this, distance);
	Pickup* closestPickup = nullptr;

	if (closestObject != nullptr)
	{
		if (closestObject->objectTag == "Pickup")
		{
			closestPickup = static_cast<Pickup*>(closestObject);

		}

		if (closestPickup != nullptr)
		{
			//std::cout << "closest pickup!" << std::endl;
			//if (CheckCollisionWith(*closestPickup) == 3)
			//{
			//worldInstance->DeleteObject(closestPickup);
			//std::cout << "closest pickup collision die!" << std::endl;
			//activate function in pickup passing the object(UI coi

			if (closestPickup)
			{
				float distance;
				worldInstance->GetClosestObject(closestObject, distance); // simle collision check by distance
				if (distance < 0.8f)
					closestPickup->isPickedUp = true;
			}

			//}
		}
	}

	if (objTransform->GetPosition().y < floorHight)
	{
		velocity.y = -0.5f * velocity.y;
		if (velocity.y < 0.01)
		{
			isGrounded = true;
			objTransform->SetPosition(glm::vec3(objTransform->GetPosition().x, floorHight, objTransform->GetPosition().z));
			//velocity.y = 0;
		}
	}
	else
	{
		if (worldInstance == nullptr) { return; }
		float distance;
		GameObject* closestObject = worldInstance->GetClosestObject(this, distance);
		Platform* closestPlatform = nullptr;

		if (closestObject != nullptr)
		{
			if (closestObject->objectTag == "Platform")
			{
				closestPlatform = static_cast<Platform*>(closestObject);
			}

			if (closestPlatform != nullptr)
			{
				//std::cout << "Colliding with platform!!" << std::endl;

				if (CheckCollisionWith(*closestPlatform) == 2)
				{
					floorHight = (closestPlatform->objTransform->GetPosition().y + closestPlatform->objMesh->meshBounds->topBound);// *1.7f;
																																   //std::cout << "Colliding with platform!! " << closestPlatform->objMesh->meshBounds->topBound << " Pbottom: " << this->objMesh->meshBounds->bottomBound << std::endl;
				}
				else
					floorHight = -0.4f;
			}
		}

		velocity.y -= (deltaTime * 50); // apply gravity
	}

	glm::vec3 newLocation = objTransform->GetPosition();

	//velocity.y +=  (deltaTime * speed);

	//std::cout << velocity.y << std::endl;

	if (velocity != glm::vec3(0.0f, 0.0f, 0.0f))
	{
		newLocation = objTransform->GetPosition() + (velocity * deltaTime);
		objTransform->SetPosition(glm::vec3(objTransform->GetPosition().x, newLocation.y, objTransform->GetPosition().z));
	}


}
