#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp> // Maths library

#include "Transform.h"

//class Transform;

class Camera
{
private:
	glm::vec3 camPosition;
	glm::vec3 camTarget;
	glm::vec3 camUp;



public:
	Camera();
	~Camera();
	Transform camTransform;

	glm::mat4 GetViewMatrix();
	glm::mat4 GetProjectionMatrix();

	void RotateCameraY(float value);
	void RotateCameraX(float value);
	void MoveForward(float value);
	void MoveRight(float value);
	void MoveUp(float value);
};


