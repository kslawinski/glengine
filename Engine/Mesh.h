#pragma once
// Include GLEW
#include <GLEW/glew.h>

// Include GLFW
#include <GLFW\glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp> // Maths library

#include <vector>

class ObjectBounds
{
private:
	std::vector<glm::vec3> objectVertices;

public:
	ObjectBounds(std::vector<glm::vec3> objectVertices)
	{
		this->objectVertices = objectVertices;
		InitializeBoundaries();
	}

	inline void InitializeBoundaries()
	{
		float lastRightBoundary = 0.0f;
		float lastLeftBoundary = 0.0f;
		float lastFrontBoundary = 0.0f;
		float lastBackBoundary = 0.0f;
		float lastTopBoundary = 0.0f;
		float lastBottomBoundary = 0.0f;

		glm::vec3 tempPosition = glm::vec3(0, 0, 0);

		for (int i = 0; i < objectVertices.size(); i++)
		{
			// get right boundary
			if (objectVertices[i].x > lastRightBoundary)
			{
				lastRightBoundary = objectVertices[i].x;
			}

			// get left boundary
			if (objectVertices[i].x < lastLeftBoundary)
			{
				lastLeftBoundary = objectVertices[i].x;
			}

			// getFront boundary
			if (objectVertices[i].z > lastFrontBoundary)
			{
				lastFrontBoundary = objectVertices[i].z;
			}

			//getBack boundary
			if (objectVertices[i].z < lastBackBoundary)
			{
				lastBackBoundary = objectVertices[i].z;
			}

			// get top boudary
			if (objectVertices[i].y < lastTopBoundary)
			{
				lastTopBoundary = objectVertices[i].y;
			}

			// get bottom baundary
			if (objectVertices[i].z < lastBottomBoundary)
			{
				lastBottomBoundary = objectVertices[i].z;
			}

		}

		// save to temp position all values before returning
		rightBound = lastRightBoundary;// * 0.5f;
		leftBound = lastLeftBoundary;// * 0.5f;
		frontBound = lastFrontBoundary;// * 0.5f;
		backBound = lastBackBoundary;// * 0.5f;
		topBound = -lastTopBoundary;// * 0.5f;
		bottomBound = -lastBottomBoundary;// *0.5f;
	}

	float rightBound;
	float leftBound;
	float frontBound;
	float backBound;
	float topBound;
	float bottomBound;
	//glm::vec3 topBound;
	//glm::vec3 bottomBound;
};

class Vertex;

class Mesh
{

private:


public:

	std::vector<glm::vec3> verticesPosition;
	std::vector<glm::vec3> verticesColour;
	std::vector<glm::vec2> verticesUV;	// ADD  NORMAL INFO

	GLuint vertices; // Declare Special variables to be used to send data to GPU
	GLuint colours;
	GLuint normals;
	GLuint uvbuffer;
	GLuint vaoHandle; // This needs to be moved to STATIC MESH
	unsigned int verticesCount;

	//TODO make it private and make GetMeshBounds function
	ObjectBounds*  meshBounds;

	bool isWireFrame = false;

	Mesh(Vertex* verticesObj, unsigned int verticesCount); // Take Object Vertex as first argument and How many vertices this object will have (Class Default Constructor)

	void Setup(); // Setup the VAO

	void Draw(); // Draw the object

	void SetMeshColour(glm::vec3 colour);

	static void Test();

	~Mesh(); // Mesh class destructor method called when the object is destroyed

	Mesh();
};