#include "Pickup.h"
#include "World.h"
#include "BoxCollider.h"
#include "Material.h"
#include "Camera.h"
#include "Transform.h"
#include <iostream>

Pickup::Pickup(World* world)
{
	Init(*world->mainCamera);
	worldInstance = world;

	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals; // Won't be used at the moment.
	bool res = loadOBJ("EngineContent/Models/cat.obj", vertices, uvs, normals);

	std::vector<Vertex> modelData;

	for (int i = 0; i < vertices.size(); i++)
	{
		modelData.push_back(Vertex(vertices[i], glm::vec3(1.0f, 0.0f, 0.0f), uvs[i]));
	}

	objMesh = new Mesh(&modelData[0], vertices.size());

	objMesh->SetMeshColour(glm::vec3(1, 1, 1));

	objectTag = "Pickup";

	//myTexture = loadBMP_custom("cat.bmp");

	SetMaterial(new Material("EngineContent/Textures/cat.bmp"));

	//objTransform->SetScale(glm::vec3(2, 2, 2));

	//objMesh->wireFrame = true;

	boxCollider = new BoxCollider(glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.0f, 0.5f, 0.5f));

	objMesh->meshBounds->InitializeBoundaries(); // update Object Boundaries
}

void Pickup::Update(float deltaTime)
{
	GameObject::Update(deltaTime);

	if (isPickedUp == false)
	{
		//boxCollider->Update(model);
		// rotate pickup
		//objTransform->SetRotation(objTransform->GetRotation() + glm::vec3(0.0f, 10.0f * deltaTime, 0.0f)); // Rotate around Y Axis
		objTransform->RotateYaw(2.0f * deltaTime); // Rotate around Y Axis
		float colsine = glm::sin(objTransform->GetEulerAngleRotation().y);

		//std::cout << "colsine: " << colsine << std::endl;
		objMesh->SetMeshColour(glm::vec3(1, colsine, 1));
	}
	else
	{
		float xpos = 10; // UI COIN SCREEN POS
		float ypos = 520;

		glm::vec3 ray_origin;
		glm::vec3 ray_direction;
		worldInstance->ScreenPosToWorldRay(
			xpos, ypos,
			960, 540,
			ray_origin,
			ray_direction
		);

		Setup(glm::normalize((ray_origin + (ray_direction)) - objTransform->GetPosition()), 15);

		glm::vec3 newLocation = objTransform->GetPosition();

		bulletVelocity += bulletDirection * (deltaTime * bulletSpeed);

		//std::cout << velocity.y << std::endl;

		if (bulletVelocity != glm::vec3(0.0f, 0.0f, 0.0f))
		{
			newLocation = objTransform->GetPosition() + (bulletVelocity * deltaTime);
			objTransform->SetPosition(glm::vec3(newLocation));
		}

		if (objTransform->GetScale().x > 0.08f)
			objTransform->SetScale((objTransform->GetScale() - (glm::distance(objTransform->GetPosition(), (ray_origin + (ray_direction))) / 10) * deltaTime));

		if (glm::distance(objTransform->GetPosition(), (ray_origin + (ray_direction))) < 0.1f)
		{
			worldInstance->DeleteObject(this);
			std::cout << "DELETE!!!" << std::endl;
			worldInstance->playerPoints += 1;
		}
	}

}

Pickup::~Pickup()
{
	std::cout << "Object Destroyed!" << std::endl;
	delete objShader;
	//glDeleteTextures(1, &myTexture);
}

void Pickup::Draw()
{
	GameObject::Draw();
	boxCollider->Update(model);
}

void Pickup::Setup(glm::vec3 direction, float speed)
{
	bulletDirection = direction;
	bulletSpeed = speed;
}
