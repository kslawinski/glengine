#pragma once
#include <glm/glm.hpp>

#include "Shader.h"
#include "Mesh.h"
#include "Transform.h"

#include <vector>
#include <cstring>
#include <iostream>


class UIObject
{
private:
	unsigned int Text2DTextureID;
	unsigned int Text2DVertexBufferID;
	unsigned int Text2DUVBufferID;
	unsigned int Text2DShaderID;
	unsigned int Text2DUniformID;
public:
	Shader * objShader;

	//int index = 0;

	std::vector<glm::vec2> vertices;
	std::vector<glm::vec2> UVs;

	Transform*  objTransform;
	Mesh* objMesh;

	GLuint myTexture;

	UIObject();
	~UIObject();

	void initText2D(const char * texturePath);
	void printText2D(const char * text, int x, int y, int size);
	void printSprite(int index, int x, int y, int size);
	void cleanupText2D();

	GLuint loadBMP_custom(const char * imagepath);
	GLuint loadDDS(const char * imagepath);
};

