#include "Vertex.h"

Vertex::Vertex()
{
}


Vertex::Vertex(glm::vec3 position, glm::vec3 colour, glm::vec2 uvCord)
{
	// Vertex class default constructor method, called automaticly when the object is created
	this->position = position;
	this->colour = colour;
	this->uvCord = uvCord;
}

Vertex::~Vertex()
{
}