#pragma once
// Include GLEW
#include <GLEW/glew.h>
#include <vector>
#include <cstdio>
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

class Shader
{
public:
	Shader();
	Shader(std::string vs, std::string fs);
	~Shader();

	void Bind();

	GLuint shaderProgram;

	GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);  // LOAD SHADERS FROM FILES
};

