#pragma once
#include "GameObject.h"
class Pickup :
	public GameObject
{
public:
	Pickup();
	Pickup(World* world);
	void Update(float deltaTime) override;
	~Pickup();

	void Draw() override;

	void Setup(glm::vec3 direction, float speed);

	class BoxCollider* boxCollider;

	bool isPickedUp = false;

private:


	glm::vec3 bulletDirection;
	float bulletSpeed;
	glm::vec3 bulletVelocity;
};