#pragma once
// Include GLEW
#include <GLEW/glew.h>

// Include GLFW
#include <GLFW\glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp> // Maths library

#include <vector>

class Material;
class Mesh;
class Transform;

class StaticMesh
{
private:



public:
	GLuint vertices; // Declare Special variables to be used to send data to GPU
	GLuint colours;
	GLuint normals;
	GLuint uvbuffer;
	GLuint vaoHandle;
	unsigned int verticesCount;

	std::vector<glm::vec3> verticesPosition;
	std::vector<glm::vec3> verticesColour;
	std::vector<glm::vec2> verticesUV;	// ADD  NORMAL INFO

										//move to private later
	Material* objMaterial;
	Mesh* objMesh;
	Transform* objTransform;

	StaticMesh();
	~StaticMesh();

	void Setup();
	void SetMaterial(Material& material);
	void SetMesh(Mesh& mesh);
	void Draw();
};