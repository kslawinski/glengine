#version 430
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 col;
layout(location = 2) in vec3 norms;
layout(location = 3) in vec2 vertexUV;

// Output data ; will be interpolated for each fragment.
out vec2 UV;

out vec3 LightIntensity;

uniform vec3 LightPosition; //Position in eye coords
uniform vec3 Kd; //Diffuse Reflectivity
uniform vec3 Ks; //Specular Reflectivity
uniform vec3 Ka; //Ambient Intensity
uniform vec3 Ld; //Light source intensity
uniform mat4 ModelView;
uniform mat3 Norm;
uniform mat4 MVP;

void main(){
 vec3 tnorm = normalize(Norm * norms);
 vec3 eyeCoords = (ModelView * vec4(pos, 1.0f)).xyz;
 vec3 s = normalize(LightPosition-eyeCoords);
 //LightIntensity = Ld * Kd * max(dot(s,tnorm),0.0f) +
//Ka;
 LightIntensity = Ld * Kd * col * max(dot(s,tnorm),0.0f) +
Ka * col;
vec3 r = reflect(-s,tnorm);
 LightIntensity += Ld * Ks * pow(max(dot(-normalize(eyeCoords),r),0.0f),30);
 gl_Position = MVP * vec4(pos, 1.0f);
 
 	// UV of the vertex. No special space for this one.
	UV = vertexUV;
}
